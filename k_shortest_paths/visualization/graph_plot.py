import networkx as nx
import holoviews as hv
from k_shortest_paths.common.digraph_utils import path_edge_list
from bokeh.models import Range1d, Plot, Circle, MultiLine, GraphRenderer, CustomJS, ColumnDataSource, LabelSet
from bokeh.models import HoverTool, BoxZoomTool, WheelZoomTool, ResetTool, SaveTool, PanTool
from bokeh.models.graphs import NodesAndLinkedEdges, NodesOnly, EdgesAndLinkedNodes
from bokeh.models.widgets import Slider
from bokeh.layouts import column
from bokeh.io import output_file, show
from bokeh.palettes import Inferno
from bokeh.transform import linear_cmap
hv.extension('bokeh')

def draw_graph_k_paths(graph, k_paths, layout_type = nx.spring_layout, palette = Inferno[11], weight = "weight"):
    """Creates an interactive plot of a network graph with highlighted paths.

    The following interactions are possible:
    - Switch between the hightlighted paths (only  one path is highlighted at
      once)
    - Zoom, Pan, Reset, Save plot
    - Hover over edges to see their weights.

    The name of the nodes are displayed in a label on the upper right side of
    the nodes if there are less than 100 nodes in the graph. For bigger graphs
    the labels are ommitted to still be able to see the nodes.

    Parameters
    ----------
    graph: nx.DiGraph
        A network graph that should be displayed
    k_paths: list [list [node]]
        the paths that should be highlighted. A path is represented by a list of
        nodes. When calling this function it should be ensure by the caller that
        all of these nodes have to be contained in `graph`
    layout_type: a layout from networkx.drawing.layout
        Determines the coordinates of the nodes in the plot. The default layout
        would be the spring layout where edges behave like springs.
    palette: bokeh pallete
        See https://docs.bokeh.org/en/latest/docs/reference/palettes.html for
        details
        This determines the colors of nodes and edges.
    """

    k = len(k_paths)

    # Rename nodes to work with integers
    # (Original node names will be stored in the attribute name and later in
    # the data source of the node renderer such that they are accessible for the hover tool)
    graph = nx.convert_node_labels_to_integers(graph, first_label=0, label_attribute='name')

    # Rename nodes in paths accordingly
    node_name_map = {graph.nodes[i]['name']: list(graph.nodes)[i] for i in range(len(graph.nodes))}####
    k_converted_paths = [[ node_name_map[v] for v in path] for path in k_paths]

    plot = Plot(x_range=Range1d(-2, 2), y_range=Range1d(-2, 2))

    ARROWHEAD_LENGTH = 0.025
    if graph.number_of_nodes() > 100:
        ARROWHEAD_LENGTH = 0.005
    # Create Holoviews graph in order to add arrowheads on directed edges
    hvGraph = hv.Graph.from_networkx(graph, layout_type, scale=1, center=(0,0)).opts(tools=['hover'],
                                     directed=True, arrowhead_length=ARROWHEAD_LENGTH, inspection_policy='edges')
    hvRendered = hv.render(hvGraph, 'bokeh')

    # Create bokeh renderer
    graph_renderer = hvRendered.select_one(GraphRenderer)


    # ----- Prepare data for visual information -----
    # We want to determine what color each node and each edge gets and how thick the lines are.
    # The corresponding ColumnDataSource will be passed to the JS callback so we can change the colors if
    # another path should be highlighted

    # Initialize with empty lists
    data_visual_attributes = {'node_colors_paths': [[] for i in range(k)],
                              'edge_colors_paths': [[] for i in range(k)],
                              'line_widths_paths': [[] for i in range(k)]}
    source_visual_attributes = ColumnDataSource(data=data_visual_attributes)

    # Extract the actual data from the given graph and the k paths and
    # fill it in the just created data source
    prepare_data(graph_renderer, graph, k_converted_paths, source_visual_attributes, palette)


    # ----- Highlight the given path -----
    # (Link the data to the node and edge renderers)
    color_selected_path(graph_renderer, palette, graph.number_of_nodes())


    # ----- Add Tools to plot for interactive examination -----

    graph_renderer.inspection_policy = EdgesAndLinkedNodes()

    graph_renderer.node_renderer.data_source.data['name'] = [graph.nodes[i]['name'] for i in range(len(graph.nodes))]
    #hover_nodes = HoverTool(tooltips=[("index", "@index"), ("name", "@name")]) #works with inspection_policy NodesOnly()

    graph_renderer.edge_renderer.data_source.data['name_total'] = [(str(graph.nodes[u]['name'])+' -> '+str(graph.nodes[v]['name'])) for
                                                                   (u,v) in graph.edges]
    graph_renderer.edge_renderer.data_source.data[weight] = [graph.edges[u,v][weight] for (u,v) in graph.edges]

    hover_edges = HoverTool(tooltips=[("edge", "@name_total"), ("weight", "@weight")])

    #alternatively: split start and end node's name in two separate attributes
    #graph_renderer.edge_renderer.data_source.data['start_name'] = [graph.nodes[u]['name'] for (u,v) in graph.edges]
    #graph_renderer.edge_renderer.data_source.data['end_name'] = [graph.nodes[v]['name'] for (u,v) in graph.edges]
    #hover_edges = HoverTool(tooltips=[("start", "@start_name"), ("end", "@end_name"), ("weight", "@weight")])

    plot.add_tools(hover_edges, PanTool(), BoxZoomTool(), WheelZoomTool(), ResetTool(), SaveTool())

    plot.renderers.append(graph_renderer)


    # ----- Add node labels -----

    if graph.number_of_nodes() <= 50:
        x, y = zip(*graph_renderer.layout_provider.graph_layout.values())
        x = [i+0.025 for i in x]
        y = [i+0.025 for i in y]
        node_labels = nx.get_node_attributes(graph, 'name')
        label_source = ColumnDataSource({'x': x, 'y': y,
                               'name': [node_labels[i] for i in range(len(x))]})
        labels = LabelSet(x='x', y='y', text='name', source=label_source)
        plot.renderers.append(labels)


    # Create container for plot and slider to have both in one html output
    graphic_elements = plot


    # ----- Create and configure Slider -----
    # (lets us switch between highlighted paths if there is more than one)

    if k > 1:
        slider = add_slider(k, graph_renderer, source_visual_attributes)
        graphic_elements = column(slider, plot)

    show(graphic_elements)



def draw_graph_highlighted_path(graph, highlighted_path,
                                layout_type = nx.spring_layout, palette = Inferno[11]):
    '''Plots a graph and highlights a path of that graph.
    Highlighted_path should be a list of nodes in graph which form a valid path
    in that graph

    '''
    draw_graph_k_paths(graph, [highlighted_path], layout_type = layout_type, palette = palette)



def prepare_data(graph_renderer, graph, k_paths, source_visual_attributes, palette):

    prepare_node_data_k_paths(source_visual_attributes, graph, k_paths)
    prepare_edge_data_k_paths(source_visual_attributes, graph, k_paths, palette)
    select_path(graph_renderer, source_visual_attributes, 0)



def prepare_node_data_k_paths(source_visual_attributes, graph, k_paths):

    k = len(k_paths)

    COLOR_PATH_START_END_NODE = 30
    COLOR_PATH_MIDDLE_NODE = 50
    COLOR_NON_PATH_NODE = 85

    # store all color values for all graphs of the k_paths list in the data source for visual attributes
    # (we have to store it there so we can access it in the slider's JS callback)
    for current_path_number in range(k):

        highlighted_path = k_paths[current_path_number]

        source_visual_attributes.data['node_colors_paths'][current_path_number] = [
                    COLOR_PATH_START_END_NODE if i == highlighted_path[0] or i == highlighted_path[-1]
                    else COLOR_PATH_MIDDLE_NODE if i in highlighted_path
                    else COLOR_NON_PATH_NODE for i in list(graph.nodes())
                    ]

    return source_visual_attributes

def prepare_node_data(source_visual_attributes, graph, highlighted_path):
    '''Adds a color attribute to the node renderer so that the color
    can be set according to that attribute.
    Nodes belonging to the highlighted path will get a different
    attribute value than the nodes outside the path.
    The exact color depends on the palette used.
    '''
    prepare_node_data_k_paths(source_visual_attributes, graph, [highlighted_path])


def prepare_edge_data_k_paths(source_visual_attributes, graph, k_paths, palette):

    k = len(k_paths)

    COLOR_PATH_EDGES = palette[int(len(palette)*50/100)]
    COLOR_NON_PATH_EDGES = palette[int(len(palette)*0/100)]

    WIDTH_PATH_EDGES = 3
    WIDTH_NON_PATH_EDGES = 0.5
    if graph.number_of_nodes() >= 50:
            WIDTH_PATH_EDGES = 1
            WIDTH_NON_PATH_EDGES = 0.1


    for current_path_number in range(k):

        highlighted_path = k_paths[current_path_number]
        path_edges = path_edge_list(highlighted_path) # from digraph_utils
        # TODO: check if path is contained in graph?

        source_visual_attributes.data['edge_colors_paths'][current_path_number] = [
                    COLOR_PATH_EDGES if e in path_edges
                    else COLOR_NON_PATH_EDGES for e in list(graph.edges())
                    ]

        source_visual_attributes.data['line_widths_paths'][current_path_number] = [
                    WIDTH_PATH_EDGES if e in path_edges
                    else WIDTH_NON_PATH_EDGES for e in list(graph.edges())
                    ]

    return source_visual_attributes


def prepare_edge_data(source_visual_attributes, graph, highlighted_path, palette):
    '''Adds a color attribute to the edge renderer so that the color
    can be set according to that attribute.
    Edges belonging to the highlighted path will get a different
    attribute value than the edges outside
    the path. The exact color depends on the palette used.
    '''
    prepare_edge_data_k_paths(source_visual_attributes, graph, [highlighted_path], palette)


def select_path(graph_renderer, source_visual_attributes, i):
    graph_renderer.edge_renderer.data_source.data['edge_colors'] = source_visual_attributes.data['edge_colors_paths'][i]
    graph_renderer.edge_renderer.data_source.data['line_widths'] = source_visual_attributes.data['line_widths_paths'][i]
    graph_renderer.node_renderer.data_source.data['node_colors'] = source_visual_attributes.data['node_colors_paths'][i]



def color_selected_path(graph_renderer, palette, node_count):

    color_nodes(graph_renderer, palette, node_count)
    color_edges(graph_renderer)



def color_nodes(graph_renderer, palette, node_count):
    '''Colors the nodes of the graph according to the attribute 'node_color' using the given palette.

    '''
    node_size = 7
    if node_count >= 50:
        node_size = 4
        if node_count >= 100:
            node_size = 1
            if node_count >= 200:
                node_size = 0.5
                if node_count > 500:
                    node_size = 0.2

    graph_renderer.node_renderer.glyph = Circle(
                size=7,
                fill_color=linear_cmap('node_colors', palette, 0, 100)
                )

def color_edges(graph_renderer):
    '''Colors the edges of the graph according to the attribute 'edge_color' using the given palette.
    Additionally, edges of the highlighted path get the width in correspondence to the 'line_width' attribute.

    '''
    graph_renderer.edge_renderer.glyph = MultiLine(line_color='edge_colors', line_width='line_widths')




def add_slider(k, graph_renderer, source_visual_attributes):
    ''' Creates a slider that allows the user to switch between different highlighted paths in the same graph.
    The value of the slider is the number of the path displayed.
    '''

    # data sources that the following java script code in the callback can access and change
    source_nodes = graph_renderer.node_renderer.data_source
    source_edges = graph_renderer.edge_renderer.data_source

    change_path_callback = CustomJS(args=dict(source_nodes=source_nodes, source_edges=source_edges,
                                              source_visual_attributes=source_visual_attributes), code="""
        var newPathNumber = cb_obj.value - 1;
        source_nodes.data['node_colors'] = source_visual_attributes.data['node_colors_paths'][newPathNumber]
        source_edges.data['edge_colors'] = source_visual_attributes.data['edge_colors_paths'][newPathNumber]
        source_edges.data['line_widths'] = source_visual_attributes.data['line_widths_paths'][newPathNumber]

        source_nodes.change.emit();
        source_edges.change.emit();
    """)

    slider = Slider(start=1, end=k, value=1, step=1, title="Path")
    slider.js_on_change('value', change_path_callback)

    return slider
