"""This module provides some data structures and functions that are used in
Eppstein's k-shortest-paths algorithm.

The names of the functions and variables are taken from the paper describing the
algorithm.

"""
import networkx as nx
from k_shortest_paths.common.digraph_utils import head, tail, length_of_edge, path_edge_list
from k_shortest_paths.common.digraph_utils import single_source_shortest_path_tree_T
import k_shortest_paths.common.dijkstra as dijkstra
from k_shortest_paths.common.tree import Tree
from heapq import heapify, heappop, heappush
import heapq
import ast
import math

class EppsteinEnvironment:
    """This class provides functionality to execute the Eppstein algorithm
    for finding the k shortest paths between two nodes s and t in a directed
    Graph. An instance of the class has the graph data of its nodes and edges.
    Additionally, it has the attributes s and t (start and end node) and k (the
    number of shortest paths to find).
    Every step of the Eppstein algorithm can be performed using a method of this
    class. After the execution of each step the data for the next step will be
    stored in attributes of the instance of this `k_shortest_paths.eppstein.eppstein_utils.EppsteinEnvironment`.
    This data includes:
     - the shortest path tree T with destination t
     - the heaps H_out(v) of outgoing non tree edges for all nodes v
     - the marked heaps H_T(v) of all outroots on the shortest path from v to
       `t` for all nodes v in T
     - the path graph P_G
     - (the heap H_G of paths in P_G starting from its root - using the old
        implementation)
     - the implicit representations of the k shortest paths k_implicit_paths

    The names of the methods and variables are closely based on the
    denotations in the paper.

    Make sure to provide valid input. The nodes `s` and `t` should be contained
    in the graph `G`. Also there should exist an s-t-path in `G`, nodes should
    have unique names and there should not be multiple edges between nodes.
    """

    def __init__(self, G: nx.DiGraph, s, t, k, weight='weight'):
        """Initializes an EppsteinEnvironment, setting the given parameteres
        as attributes
        """
        self.G = G # directed graph
        self.s = s # start
        self.t = t # destination
        self.k = k # number of shortest paths we want to find
        self.weight = weight
        self.n = G.number_of_nodes()
        self.m = G.number_of_edges()
        self.deltas = {}


    # --------------------------------------------------------------------------
    # Some helper functions:
    # --------------------------------------------------------------------------

    def delta(self, edge):
        """Returns the precomputed output of the delta function from the paper
        of the given edge

        The delta function of an edge specifies the sum of weights which is
        added to an s-t-paths when adding `edge` to a path. So to speak it is
        the length of the detour that has to be taken when taking `edge`.
        We only compute this for edges in G-T since edges in T don't lengthen a
        path, so delta(e) = 0 for all e in T.

        The outputs of this function can later be used for sorting the edges in
        heaps.
        Since we want to find the k shortest paths we want to find the
        "smallest detours" from the shortest paths, so the edges with small
        delta values.

        Parameters
        ----------
        edge: tuple (v1, v2)
            edge of the digraph G

        Returns
        -------
        int
            The delta value of the given `edge`
        """
        #delta = length_of_edge(edge, self.G, self.weight) + self.distance_to_t(head(edge)) - self.distance_to_t(tail(edge))
        return self.deltas[edge]

    def compute_delta(self, edge):
        """Computes the output of the delta function from the paper of the
        given edge and stores it.

        It gets stored as a key value pair edge: delta(edge) in the dict
        attribute self.delta

        The delta function of an edge specifies the sum of weights which is
        added to an s-t-paths when adding `edge` to a path. So to speak it is
        the length of the detour that has to be taken when taking `edge`.
        We only compute this for edges in G-T since edges in T don't lengthen a
        path, so delta(e) = 0 for all e in T.

        The outputs of this function can later be used for sorting the edges in
        heaps.
        Since we want to find the k shortest paths we want to find the
        "smallest detours" from the shortest paths, so the edges with small
        delta values.

        Parameters
        ----------
        edge: tuple (v1, v2)
            edge of the digraph G

        Returns
        -------
        int
            The delta value of the given `edge`
        """
        delta = length_of_edge(edge, self.G, self.weight) + self.distance_to_t(head(edge)) - self.distance_to_t(tail(edge))
        self.deltas[edge] = delta
        return delta

    def next_T(self, node):
        """Returns the next node from a given node on the path along T towards t.

        Parameters
        ----------
        node: node
            A node in `G`

        Returns
        -------
        node
            The parent in T of the given node, so the next node on shortest path
            from `node`to `t`. None if given node is `t` or `node` is not
            contained in T
        """
        if node in self.T:
            next_node_in_T = self.T.get_parent(node)
        else:
            next_node_in_T = None
        return next_node_in_T


    def distance_to_t(self, node):
        """Returns length of the shortest path from node to t.

        Parameters
        ----------
        node:
            starting node

        Returns
        -------
        int
            distance between 'node' and t

        """
        d = 0
        if (node != self.t):
            if node in self.distances_to_t:
                d = self.distances_to_t[node]
            else:
                d = math.inf
        return d


    def sidetracks(self, path):
        """Extracts and returns the edges from the given path that are not contained in T
        (the single-destination shortest path tree with destination t).

        Taking a sidetrack edge in a path between two nodes means therefore taking a detour.
        Since we want to not only find the shortest but the k shortest paths we have to
        examine these detours and find the shortest ones.

        Parameters
        ----------
        path: list of nodes
            A path in `G`

        Returns
        -------
        list of edges
            the sidetracks contained in `path`. An edge is a 2-tuple of two
            nodes
        """
        path_edges = path_edge_list(path) #from digraph_utils
        sidetracks = [(v1,v2) for (v1,v2) in path_edges if v2 != self.next_T(v1)]
        return sidetracks


    def path(self, sidetracks):
        """Returns explicit s-t-path given its implicit representation through
        sidetracks.

        The given sidetracks are those edges in p that are not cointained in T.
        Since the edges in T are known this uniquely represents an explicit path
        by adding the edges in T between the sidetrack's nodes.

        Note:
        If the list of sidetracks is not a valid implicit representation of an
        s-t-path it could happen that this method does not terminate.

        Parameters
        ----------
        sidetracks: list of edges (2-tuples)
            list of non-T-edges defining an implicit representation of an
            s-t-path

        Returns
        -------
        list of nodes
            the explicit representation of the s-t-path defined by the given
            sidetracks
        """

        #TODO: is it maybe possible to use prefpath to speed method up???
        # Right now, for each implicit path we traverse the whole Graph in
        # worst case (O(n))

        p = [self.s]
        current_node = self.s
        if sidetracks:
            next_sidetrack = sidetracks.pop(0)
        else:
            next_sidetrack = (None,  None)

        while current_node != self.t:

            if current_node == tail(next_sidetrack):
                # follow sidetrack edge
                current_node = head(next_sidetrack)
                # check which is the next sidetrack to come
                if sidetracks:
                    next_sidetrack = sidetracks.pop(0)
                else:
                    next_sidetrack = (None,  None)
            else:
                #in this case there is no sidetrack tailing in the current node
                #so continue taking shortest path along T
                current_node = self.next_T(current_node)

            p.append(current_node)

        return p


    def prefix(self, sidetracks):
        """Returns list of sidetracks without the last edge.
        """
        prefix = sidetracks.copy()
        prefix.pop()
        return prefix


    def prefpath(self, path):
        """The "prefix path" is determined as follows:
        Given a path p we determine the sidetracks contained in p,
        thus the edges that are not in the single-destination shortest path tree T.
        Then we delete the last sidetrack cut off the last sidetrack.
        The prefix path is now the path uniquely defined by this new (shorter)
        list of sidetracks.

        """
        return path(prefix(sidetracks(path)))


    def lastedge(self, path):
        """Returns the last sidetrack edge in the given path,
        thus the last edge in the path that is not contained in
        the single-destination shortest path tree T.
        """
        last_edge = sidetracks(path)[-1]
        return last_edge


    def out(self, node):
        """Returns the unordered list of valid sidetrack edges starting from
        the given `node`, together with their corresponding delta values.

        Sidetrack edges are edges in G-T.

        Only those edges which are leading to a node w that is contained in T
        will be returned. If the destination node w is NOT in T, it means that
        there is no path from w to `t`, so this sidetrack cannot be contained in
        any s-t-path, and is hence not of interest for our algorithm.

        Parameters
        ----------
        node: node
            A node in `G`

        Returns
        -------
        list
            elements are 2-tuples, where first element is the delta value and
            the second the corresponding edge in G-T.
            Returns empty list [] if there are no outgoing edges in G-T
            from `node`
        """

        next_node = self.next_T(node)
        out_edges = [ [self.compute_delta((node, successor_node)),(node, successor_node)] for
               successor_node in self.G.successors(node) if
               successor_node != next_node and self.T.has_node(successor_node)]
        return out_edges


    def outroot(self, node):
        """Return outroot of this node.

        The outroot of a node v is the outgoing edge from v with the
        smallest delta value. It is the root of the heap H_out(v).

        When calling this method it should be ensured that the H_outs were
        already created.
        """
        if self.H_out[node]:
            return self.H_out[node].out_root
        else:
            return None


    # --------------------------------------------------------------------------
    # Implementations of Eppstein steps:
    # --------------------------------------------------------------------------

    def create_single_destination_shortest_path_tree_T(self):
        """Calculates the single-destination shortest path tree T
        with destination 't'.
        The resulting tree is not root-directed, so all the edges are
        reversed compared to the original graph.
        """
        # reverse graph and apply single source dijkstra on it with source t
        G_R = self.G.reverse()
        # note: since we use reversed  graph, edges in T are reversed, too!
        self.T, self.distances_to_t = single_source_shortest_path_tree_T(G_R, self.t, self.weight)


    def create_H_out(self, node):
        """Creates the heap H_out for the given node and stores it as an
        attribute of this Eppstein graph.

        The heap H_out(v) contains all outgoing non tree edges
        """

        self.H_out[node] = OutHeap(self.out(node))


    def create_all_H_out(self):
        """Creates all heaps H_out(v) of outgoing non tree edges from a `T` node
        for all nodes in `T`.

        The weights used in the heaps are the delta values of the edges.

        The heaps also get stored as an attribute of this `EppsteinEnvironment`.

        Returns
        -------
        dict {node: `eppstein_utils.OutHeap`}
            The nodes of T paired with their heaps H_out. The heaps are
            instances of the class OutHeap with an outroot and an heap as
            attributes.
        """
        self.H_out = {}
        for v in self.T.nodes:
            # nodes in G-T are not interesting because we cannot reach t anymore
            # from there. So it is sufficient to only consider nodes in T
            self.create_H_out(v)

        return self.H_out


    def create_all_H_T(self):
        """Build and returns heaps H_T(v) for all v in G.

        According to the algorithm, those heaps contain the outroots
        of all nodes on the path from v to t in T, heap-ordered by their delta values.

        For convenience and later easier accessability we store the names
        of nodes v in the heaps instead of outroot(v).
        We build the heaps by traversing the shortest paths tree T from
        destination t backwards.

        For each node v in G we build the heap by the following procedure:

        1) copy the heap of next node on the path from v to t along T
           (if there is one)
        2a) insert the current node v into this heap using the delta value
            of v's outroot as the heap-order-value
            (if v HAS an outroot - otherwise skip step 2)
        2b) keep track of the heap nodes that were altered during the insertion
            by storing the according bool values in marks_of_H_T
            (These marks are needed in the next step of the algorithm to build
            the path graph)

        The heaps also get stored as an attribute of this `EppsteinEnvironment`.

        Returns
        -------
        H_T: dict
            the keys in this dict are the nodes v of G, the values their heap H_T(v),
            represented by a list of 2-tuples. First element is delta value of outroot of node x,
            second element is node x. The heap contains all nodes x on the path from v to t along T.
        marks_of_H_T: dict
            the keys in this dict are the nodes v of G, the values are lists of bools with
            the same length as H_T(v). Value true at index i means that node at position i
            in H_T(v) got altered when creating the heap H_T(v).


        """
        H_T = {v: None for v in self.G.nodes}
        marks_of_H_T = {v: None for v in self.G.nodes}

        # BFS on T
        T_nodes_visit_next = [self.t]
        for current_node in T_nodes_visit_next:
            T_nodes_visit_next.extend(self.T.get_children(current_node))
            # since T is a tree we don't have circles so don't need to worry
            # about visiting nodes multiple times


            current_outroot = self.outroot(current_node)
            # if there is no outgoing sidetrack from the current node there is
            # no new heapitem for this node
            if current_outroot:
                current_heapitem = [self.outroot(current_node)[0], current_node]
            else:
                current_heapitem = None


            # we need to distinct different cases in the following because some
            # heaps or the new heapitem itself could be empty

            #is there a previous node or is the current node the root?
            previous_node = self.next_T(current_node)
            if previous_node:

                previous_heap = H_T[previous_node]
                if previous_heap:
                    if current_heapitem:
                        # 1):
                        H_T[current_node] = previous_heap.copy()
                        # 2):
                        marks_of_H_T[current_node] = marked_heappush(H_T[current_node], current_heapitem)
                        # element of heap is 2-tuple, first element is delta value of outroot of node v,
                        # second element is node v
                    else:
                        # 1):
                        H_T[current_node] = previous_heap
                        # 2):
                        marks_of_H_T[current_node] = [False]*len(previous_heap)
                else:
                    if current_heapitem:
                        # 1):
                        H_T[current_node] = [current_heapitem]
                        # 2):
                        marks_of_H_T[current_node] = [True]

                    #else: both previous heap and new heapitem are empty, so new heap can simply keep value None

            else: # here current_node is the root
                if current_heapitem:
                    # 1):
                    H_T[current_node] = [current_heapitem]
                    # 2):
                    marks_of_H_T[current_node] = [True]

                #else: both previous and new heapitem are empty, so new heap can simply keep value None

        self.H_T = H_T
        self.marks_of_H_T = marks_of_H_T
        return H_T, marks_of_H_T


    def create_path_graph(self):
        """Step 6 of Eppstein's algorithm. Creates the path graph for this Eppstein graph.

        Paths in this graph starting from the root r correspond to s-t-paths in G in a
        length-preserving way. Therefore, by traversing this path graph we can in Step 7
        easily create a heap of the implicit representations of the shortest s-t-paths in G.

        Returns
        -------
        P_G: nx.DiGraph
            the path graph for this Eppstein graph.
        """

        P_G = nx.DiGraph()
        h = {v: str(v) + '_' + str(self.H_T[v][0][1]) if self.H_T[v]
             else None for v in self.T.nodes }
            # in this dict there are the names of the nodes in P(G)
            # corresponding to the roots of H_T
            # in self.H_T[v][0][1]), the first index 0 takes the root of heap
            # H_T, second index takes name of node in root


        # BFS on T:
        T_nodes_visit_next = [self.t]
        for current_T_node in T_nodes_visit_next:
            T_nodes_visit_next.extend(self.T.get_children(current_T_node))

            # 1) a) Add all marked nodes from Heaps H_T(v) - representing
            #.      outroots of node v in G,
            #.   b) edges between marked nodes from a)
            #.   c) edges between marked parent-nodes from a) and the marked
            #.      version in a previous heap of the (in H_T(v)) unmarked
            #.      children-node
            #.
            #. Meanwhile, we additionally have to make sure that after step 1 the
            #. dict h has its correct final values (could happen that the root of an
            #. H_T was not marked (if the complete heap was unmarked).  We have
            #. to keep track of what is the corresponding P(G) node - when was the node previously
            #. marked (in which previous H_T?)), so we do:
            #.
            #.   d) for each unmarked root r of a heap H_T: update h[r]


            if self.H_T[current_T_node]:

                index_current_node = 0 #root


                # Find properties about root node and corresponding G-edge
                current_H_T_node = self.H_T[current_T_node][index_current_node]
                v = current_H_T_node[1]
                # v is the node in G of which outroot(v) should theoretically
                # be stored in the heap H_T. For simplicity and accessibility we stored v instead.

                if not self.marks_of_H_T[current_T_node][0]:
                    # here entire heap H_T[current_T_node] is unmarked because
                    # H_out of current node has no outroot)

                    # 1d): Update h(root)

                    # Look for the first heap in which the root node is marked
                    # by "going back the heaps" and check for marks

                    heap_with_marked_current_node = self.next_T(current_T_node)
                    while not self.marks_of_H_T[heap_with_marked_current_node][0]:
                        heap_with_marked_current_node = self.next_T(heap_with_marked_current_node)

                    h[current_T_node] = str(heap_with_marked_current_node) + '_' + str(v)

                    continue
                    # proceed to next node in T. The rest of the current
                    # heap H_T is unmarked too, if the root is already
                    # unmarked

                else:
                    # now the more interesting case: We have a marked path in
                    # the current H_T

                    current_P_node_name_prefix = str(current_T_node) + '_'
                    # the names of P_G nodes corresponding to nodes in H_T(v)
                    # all start with "v_" (This is because some edges can be
                    # represented several times in the path graph, so we have to
                    # give them different names to keep track for which H_T(v) we
                    # added them. This is also important to find the correct end
                    # nodes for the cross edges in step 3a).
                    # Later we add the name of the starting node of the
                    # corresponding outroot. So for example "b_w" would be the name
                    # of the P_G-node corresponding to the version of the
                    # outroot edge of G-node w in the heap H_T(b)

                    # read more properties of root:

                    delta = current_H_T_node[0]

                    start_G_node = self.outroot(v)[1][0]
                    end_G_node = self.outroot(v)[1][1]

                    # 1a) part 1:
                    # Add P_G-node for root of H_T[current_T_node]
                    current_node_name = current_P_node_name_prefix + str(v)
                    P_G.add_node(current_node_name, start=start_G_node, end=end_G_node)

                    length_H_T = len(self.H_T[current_T_node])

                    while current_H_T_node:

                        # find marked and unmarked child and their indices in heap
                        # and their properties (if they exist), so delta and v

                        index_left_child = (index_current_node * 2) + 1
                        index_right_child = (index_current_node * 2) + 2

                        if index_right_child < length_H_T: # -> both children exist!
                            if self.marks_of_H_T[current_T_node][index_right_child]:

                                index_marked_child = index_right_child
                                index_unmarked_child = index_left_child
                            else:
                                index_marked_child = index_left_child
                                index_unmarked_child = index_right_child
                        elif index_left_child < length_H_T: # only child has to be marked!

                            index_marked_child = index_left_child
                            index_unmarked_child = -1
                        else:
                            index_marked_child = -1
                            index_unmarked_child = -1


                        # Handle marked child:
                        if index_marked_child != -1:

                            marked_child = self.H_T[current_T_node][index_marked_child]
                            delta_marked_child = marked_child[0]
                            v_marked_child = marked_child[1]
                            start_G_node_marked_child = self.outroot(v_marked_child)[1][0]
                            end_G_node_marked_child = self.outroot(v_marked_child)[1][1]
                            marked_child_node_name = current_P_node_name_prefix + str(v_marked_child)

                            # 1a) part 2:
                            P_G.add_node(marked_child_node_name, start=start_G_node_marked_child, end=end_G_node_marked_child)
                            # 1b)
                            P_G.add_edge(current_node_name, marked_child_node_name,
                                            weight = delta_marked_child - delta, heap_edge = True) #node is not yet added!!!!!
                        else:
                            marked_child = None

                        # Handle unmarked child:
                        if index_unmarked_child != -1:
                            unmarked_child = self.H_T[current_T_node][index_unmarked_child]
                            delta_unmarked_child = unmarked_child[0]
                            v_unmarked_child = unmarked_child[1]

                            # Look for the first heap in which the unmarked
                            # children node is marked by "going back the heaps"
                            # along T and check for marks

                            heap_with_marked_child = self.next_T(current_T_node)
                            while not self.marks_of_H_T[heap_with_marked_child][index_unmarked_child]:
                                heap_with_marked_child = self.next_T(heap_with_marked_child)

                            unmarked_child_node_name = str(heap_with_marked_child) + '_' + str(v_unmarked_child)

                            # 1c):

                            P_G.add_edge(current_node_name, unmarked_child_node_name,
                                         weight = delta_unmarked_child - delta, heap_edge = True)

                        else:
                            unmarked_child = None

                        # update current node (continue exmamining H_T along the
                        # marked path downwards)
                        current_H_T_node = marked_child
                        if marked_child:
                            index_current_node = index_marked_child
                            delta = delta_marked_child
                            v = v_marked_child
                            start_G_node = start_G_node_marked_child
                            end_G_node = end_G_node_marked_child
                            current_node_name = marked_child_node_name


            # 2) a) Add all nodes of non-outroot sidetracks (heap parts of H_outs) to P_G
            #.   b) and heap edges between the nodes from 2a).

            if self.H_out[current_T_node]:
                if self.H_out[current_T_node].heap:

                    current_heap_name_prefix = str(current_T_node) + "_heap_"

                    for i, [delta, (v_in,v_out)] in enumerate(self.H_out[current_T_node].heap):

                        # 2a):

                        # add node to path graph. The name of the node in the
                        # path graph is of the format v_heap_i. This is needed
                        # to access nodes later. For example the outroot(v)
                        # that got inserted above needs to have an edge to
                        # v_heap_0. Additionally the information about the
                        # corresponding sidetrack edge in G is stored in this
                        # node in two attributes 'start' and 'end'. This will be
                        # needed later to build the k-shortest s-t-paths when
                        # traversing P(G)
                        new_node_name = current_heap_name_prefix + str(i)
                        P_G.add_node(new_node_name, start=v_in, end=v_out)

                        # 2b):

                        # add edge from parent to current node in heap
                        # (except for root of heap - the edges (can be more than 1)
                        # to the roots will be added in step 3) )
                        if i > 0:
                            index_parent = (i-1)//2
                            delta_parent = self.H_out[current_T_node].heap[index_parent][0]
                            parent_node_name = current_heap_name_prefix + str(index_parent)
                            P_G.add_edge(parent_node_name, new_node_name,
                                         weight = delta - delta_parent, heap_edge = True)

        # 3) Now we have created all nodes p in P(G). Every p corresponds to
        #.   an edge (u,w) in G. Now:
        #.
        #.   a) Add cross edges from all nodes p in P(G) to h(w), so to the
        #.      P-node corresponding to the root of the heap H_T of the
        #.      corresponding G-edge's end node w.
        #.   b) Add edges from the "outroot nodes" (outroot(v)) in P(G) from
        #.      step 1a) to the respective P(G)-nodes v_heap_0, corresponding
        #.      to the first element of their heaps H_out(v).heap (from step 2a).

        for P_node_name in P_G.nodes:

            start_G_node = P_G.nodes[P_node_name]['start']
            end_G_node = P_G.nodes[P_node_name]['end']

            # 3a):

            if self.H_T[end_G_node]:
                cross_destination_node_name = h[end_G_node]
                delta_destination = self.H_T[end_G_node][0][0] #first 0: takes root, second 0: takes its delta value
                P_G.add_edge(P_node_name, cross_destination_node_name,
                             weight = delta_destination, heap_edge = False)

            # 3b):

            if P_node_name.startswith(str(start_G_node) + '_heap'):
                continue

            if self.H_out[start_G_node].heap :

                delta_current = self.delta((start_G_node, end_G_node))
                delta_destination = self.H_out[start_G_node].heap[0][0]
                destination_node_name = str(start_G_node) + '_heap_0'
                P_G.add_edge(P_node_name, destination_node_name,
                             weight = delta_destination - delta_current, heap_edge = True)


        # 4) Add root r=r(s) to P(G) and its edge to h(s)

        root_node = 'r'
        P_G.add_node(root_node)
        if self.H_T[self.s]:
            # if there is only one s-t-path in G we don't need any edge from root in P(G)
            delta_h_s = self.H_T[self.s][0][0]
            P_G.add_edge(root_node, h[self.s], weight = delta_h_s, heap_edge = False)

        self.P_G = P_G
        self.P_G_root = root_node
        return P_G


    def create_implicit_shortest_paths_heap(self):
        """Step 7 of Eppstein's algorithm. Creates a heap of the implicit representations of
        s-t-paths in G, heap-ordered by their length.

        ************************************************************************
        NOTE: THIS METHOD DOES NOT TERMINATE FOR CYCLIC G
        And  even if G is acyclic, this method is EXTREMELY SLOW FOR BIG K.
        The runtime of this method depends exponentially from k!

        Use `EppsteinEnvironment.find_k_implicit_shortest_paths_in_P_G` instead
        ************************************************************************


        Returns
        -------
        H_G: Tree of (int, str)-nodes
            heap of implicit representations of s-t-paths in G, heap-ordered by their length
            A node in this tree is a 2-tuple where the first element is the sum of delta values
            of all sidetracks in the corresponding s-t-path. This sum added to the length of the
            shortest s-t-path add up to the total length of the corresponding s-t-path.
            The second element in the tuple is the list of sidetracks in the corresponding
            s-t-path as a str, the implicit representation of this s-t-path.
        """

        H_root = (0, '[]')
        H_G = Tree(H_root) # [] is root of H(G).
        # It corresponds to the s-t path without any sidetracks with delta value 0
        # Likewise, the name of every node in H(G) will be a list of sidetracks
        # preceeded (in the tuple) by the sum of delta value of those sidetracks



        # BFS on P(G) beginning at root r:

        P_nodes_visit_next = [[self.P_G_root, H_root]]
        # we have to traverse P(G) and H(G) at the same time, so to speak.
        # Basically, we traverse P(G), but at the same time we have to keep track of
        # where we added the nodes in H(G) so that we know where to add the next node.
        # So for every node we have to visit next in P(G) we store the name of the corresponding
        # "parent node" in H(G) (that was added for previous P-path) in a tuple when adding it to the
        # "traversing list" (this is done at (*))

        for [current_P_node, H_parent] in P_nodes_visit_next:

            H_parent_delta = H_parent[0]
            H_parent_sidetracks = ast.literal_eval(H_parent[1]) # convert str representation of list into list
            # that is the node in H(G) at which we want to add its children in this iteration

            current_P_out_edges = self.P_G.out_edges(current_P_node)

            for current_edge in current_P_out_edges:

                next_P_node = head(current_edge)
                next_G_sidetrack = (self.P_G.nodes[next_P_node]['start'], self.P_G.nodes[next_P_node]['end'])
                new_H_node_delta = H_parent_delta + self.P_G.edges[current_edge]['weight']

                if self.P_G.edges[current_edge]['heap_edge']:

                    # going along a heap edge in P(G) means that we replace
                    # the last previously added sidetrack with the new sidetrack
                    new_H_node_sidetracks = self.prefix(H_parent_sidetracks)
                    new_H_node_sidetracks.append(next_G_sidetrack)

                else:
                    # going along a cross edge or the inital edge on the other hand
                    # implies keeping all previous sidetracks and adding the new one
                    new_H_node_sidetracks = H_parent_sidetracks.copy()
                    new_H_node_sidetracks.append(next_G_sidetrack)

                new_H_node = (new_H_node_delta, str(new_H_node_sidetracks))
                H_G.add_node(new_H_node)
                H_G.add_edge(H_parent, new_H_node)

                P_nodes_visit_next.append([next_P_node, new_H_node])  #(*)

        self.H_G = H_G
        return H_G


    def create_implicit_shortest_paths_heap_reduced(self):
        """Step 7 of Eppstein's algorithm. Creates reduced heap of the implicit
        representations of s-t-paths in G, heap-ordered by their length.

        This time we don't build the entire heap, but just the k first levels.
        If our graoh G has a cylce, the path graph P(G) has cycles too. In this
        case the Heap H(G) would have infinite nodes because there are infinitely
        many paths in P(G). In order for our algorithm to terminate for cyclic
        graphs we have to only build the necessary part of H(G).

        ************************************************************************
        NOTE: THIS METHOD IS STILL EXTREMELY SLOW FOR BIG K
        The runtime of this method depends exponentially from k!

        Use `EppsteinEnvironment.find_k_implicit_shortest_paths_in_P_G` instead
        ************************************************************************

        Returns
        -------
        H_G: Tree of (int, str)-nodes
            heap of implicit representations of s-t-paths in G, heap-ordered by their length
            A node in this tree is a 2-tuple where the first element is the sum of delta values
            of all sidetracks in the corresponding s-t-path. This sum added to the length of the
            shortest s-t-path add up to the total length of the corresponding s-t-path.
            The second element in the tuple is the list of sidetracks in the corresponding
            s-t-path as a str, the implicit representation of this s-t-path.
        """

        H_root = (0, '[]')
        H_G = Tree(H_root) # [] is root of H(G).
        # It corresponds to the s-t path without any sidetracks with delta value 0
        # Likewise, the name of every node in H(G) will be a list of sidetracks
        # preceeded (in the tuple) by the sum of delta value of those sidetracks


        # BFS on P(G) beginning at root r:

        root_H_level = 1
        P_nodes_visit_next = [[self.P_G_root, H_root, root_H_level]]
        # we have to traverse P(G) and H(G) at the same time, so to speak.
        # Basically, we traverse P(G), but at the same time we have to keep track of
        # where we added the nodes in H(G) so that we know where to add the next node.
        # So for every node we have to visit next in P(G) we store the name of the corresponding
        # "parent node" in H(G) (that was added for previous P-path) in a tuple when adding it to the
        # "traversing list" (this is done at (*))


        for [current_P_node, H_parent, H_level] in P_nodes_visit_next:


            H_parent_delta = H_parent[0]
            H_parent_sidetracks = ast.literal_eval(H_parent[1]) # convert str representation of list into list
            # that is the node in H(G) at which we want to add its children in this iteration

            current_P_out_edges = self.P_G.out_edges(current_P_node)

            for current_edge in current_P_out_edges:

                next_P_node = head(current_edge)
                next_G_sidetrack = (self.P_G.nodes[next_P_node]['start'], self.P_G.nodes[next_P_node]['end'])
                new_H_node_delta = H_parent_delta + self.P_G.edges[current_edge]['weight']

                if self.P_G.edges[current_edge]['heap_edge']:

                    # going along a heap edge in P(G) means that we replace
                    # the last previously added sidetrack with the new sidetrack
                    new_H_node_sidetracks = self.prefix(H_parent_sidetracks)
                    new_H_node_sidetracks.append(next_G_sidetrack)

                else:
                    # going along a cross edge or the inital edge on the other hand
                    # implies keeping all previous sidetracks and adding the new one
                    new_H_node_sidetracks = H_parent_sidetracks.copy()
                    new_H_node_sidetracks.append(next_G_sidetrack)

                new_H_node = (new_H_node_delta, str(new_H_node_sidetracks))
                new_H_level = H_level+1
                H_G.add_node(new_H_node)
                H_G.add_edge(H_parent, new_H_node)
                if new_H_level <= self.k:
                    P_nodes_visit_next.append([next_P_node, new_H_node, new_H_level])  #(*)

        self.H_G = H_G
        return H_G


    def find_k_implicit_shortest_paths_in_P_G(self):
        """Step 7 and 8 of Eppstein's algorithm in one method.

        Returns a list of the endpoints of P_G-paths starting in r.
        Those elements are

        This method uses a prioritized breadth first search on P_G

        Returns
        -------
        list of 2-tuples (list, int)
            The length-ordered list of the implicit representations of the k
            shortest s-t-paths.
            The first element of the tuples is the length of an s-t-path, the
            second is a lists of sidetracks, implicitely representing an
            s-t-path in G.
            All these paths are ordered increasingly by their lengths (the first
            element of each tuple).

        """
        root_delta = 0
        root_sidetracks = []
        # note, that the heap H_G only does not really exist here, but we keep
        # track of the information that would be in the imaginary nodes, that is
        # the current sum of delta values on the P-path and the used sidetracks
        # up to the current P-node.

        length_shortest_path = self.distance_to_t(self.s)
        implicit_paths = []

        # prioritized BFS on P(G) beginning at root r:

        P_nodes_visit_next = [(root_delta, [self.P_G_root, root_sidetracks])] #min heap
        while P_nodes_visit_next:

            next_P_node = heappop(P_nodes_visit_next)
            current_delta_sum = next_P_node[0]
            current_P_node  =  next_P_node[1][0]
            current_G_sidetracks = next_P_node[1][1]

            # from current P_G path starting from r and ending in current_P_node
            # we now save the implicit path in G (the list of sidetracks)
            # and its length
            current_path_length = length_shortest_path + current_delta_sum
            implicit_paths.append((current_G_sidetracks, current_path_length))
            if len(implicit_paths) == self.k:
                self.k_implicit_paths = implicit_paths
                return implicit_paths

            current_P_out_edges = self.P_G.out_edges(current_P_node)

            for current_edge in current_P_out_edges:
                next_P_node = head(current_edge)
                next_sidetrack = (self.P_G.nodes[next_P_node]['start'], self.P_G.nodes[next_P_node]['end'])
                new_delta_sum = current_delta_sum + self.P_G.edges[current_edge]['weight']

                if self.P_G.edges[current_edge]['heap_edge']:

                    # going along a heap edge in P(G) means that we *replace*
                    # the last previously added sidetrack with the new sidetrack
                    new_G_sidetracks = self.prefix(current_G_sidetracks)
                    new_G_sidetracks.append(next_sidetrack)

                else:
                    # going along a cross edge or the inital edge on the other hand
                    # implies keeping all previous sidetracks and adding the new one
                    new_G_sidetracks = current_G_sidetracks.copy()
                    new_G_sidetracks.append(next_sidetrack)


                heappush(P_nodes_visit_next, (new_delta_sum, [next_P_node, new_G_sidetracks]))

        self.k_implicit_paths = implicit_paths
        return implicit_paths


    def find_k_smallest_H_G_nodes(self):
        """Step 8 of Eppstein's algorithm. Returns a list of the smallest k
        elements in the given heap of implicit paths.

        This method uses a prioritized breadth first search on the heap H(G).
        When adding a new H(G)-node to potentially visit new we use the built in
        heapq function heappush and use the weight of the H(G) as its priority.
        So we visit the nodes in H(G) in increasing order of their weights and
        add them successively to the list of the shortest paths until there are
        no paths left or we already found k ones.

        Returns
        -------
        list of 2-tuples (list, int)
            The length-ordered list of the implicit representations of the k
            shortest s-t-paths.
            The first element of the tuples is the length of an s-t-path, the
            second is a lists of sidetracks, implicitely representing an
            s-t-path in G.
            All these paths are ordered increasingly by their lengths (the first
            element of each tuple).

        """

        k_implicit_paths = []
        length_shortest_path = self.distance_to_t(self.s)

        next_H_nodes = [self.H_G.root] # priority queue realized by a min heap
        while next_H_nodes and len(k_implicit_paths) < self.k:

            current_smallest_H_node = heapq.heappop(next_H_nodes)
            current_path_length = length_shortest_path + current_smallest_H_node[0]
            k_implicit_paths.append((ast.literal_eval(current_smallest_H_node[1]), current_path_length))

            new_H_nodes = self.H_G.get_children(current_smallest_H_node)
            for v in new_H_nodes:
                heapq.heappush(next_H_nodes, v)

        self.k_implicit_paths = k_implicit_paths
        return k_implicit_paths


    def create_explicit_from_implicit_paths(self):
        """Step 9 of Eppstein's algorithm. Converts the previously calculated
        implicit represenations of the k shortest s-t-paths to explicit paths.

        Make sure that the implicit paths were already calculated
        (using `k_shortest_paths.eppstein.eppstein_utils.EppsteinEnvironment.find_k_implicit_shortest_paths_in_P_G`)

        Returns
        -------
        list of 2-tuples (list, int)
            Ordered lists of explicit k shortest s-t-paths in G paired with
            their length. First element of tuple is a list of nodes in G (so a
            path in G), second tuple element is the length of this path.

        """

        k_explicit_paths = [(self.path(implicit_path[0]), implicit_path[1])  for
                            implicit_path in self.k_implicit_paths]

        return k_explicit_paths



class OutHeap:
    """An instance of this class is used to store the edges in out(node) for a
    node of a graph `G` in an EppsteinEnvironment.

    These (G-T)-edges are heap ordered by their delta values, where edges with
    lower delta values are in higher positions in the heap.

    Basically, this data structure is almost a 2-heap. However, the root is an
    exception - the root (also called "outroot") has only one child.
    Apart from that, every G-T-edge (i.e. nodes in the heap) has (up to) 2
    children. For the heap (outroot excluded) we use the built-in heapq module.

    """

    def __init__(self, edge_delta_list):
        """Initiates an OutHeap for a given list of edges heap-ordered by their delta values

        Parameters
        ----------
        edge_delta_list: list
            list of 2-tuples of (1)delta values and (2)the corresponding edge

        """
        self.out_root = None
        self.heap = None
        if edge_delta_list:
            if len(edge_delta_list) != 0:
                heap = edge_delta_list.copy() # make sure to not change given data
                heapify(heap)
                self.out_root = heappop(heap) # remove outroot from heap and store it separately
                self.heap = heap # remaining part of heap is now what we will actually call heap
                                 #(containing all edges except for the outroot in min-heap order by delta values)

    def __str__(self):
        """Returns a string representation of this OutHeap.

        The representation looks like a list. The first element is the out_root,
        the following elements are the node of the heap.

        This is an example of a string representation of an OutHeap of a node
        called 's':
        "[[1, ('s', 'b')], [2, ('s', 'd')], [3, ('s', 'e')]]"
        Here, the outroot is the edge ('s', 'b') with delta value 1 and there
        are two more outgoing edges ('s', 'd') and ('s', 'e') with delta values
        2 and 3 stored in the heap part of this OutHeap in increasing order.
        """
        return self.__repr__()

    def __repr__(self):
        """Returns a string representation of this `OutHeap`. See
        `k_shortest_paths.eppstein.eppstein_utils.OutHeap.__str__` for
        more details.
        """
        s = []
        if self.out_root:
            s.append(self.out_root)
        if self.heap:
            s.extend(self.heap)

        return str(s)


    def pop(self):
        """Returns and removes outroot (edge with smallest delta value) from OutHeap and moves remaining heap nodes up
        """
        e = self.out_root
        if self.heap:
            self.out_root = heappop(self.heap)
        else:
            self.out_root = None
        return e



def marked_heappush(heap, item):
    """Performs an ordinary heappush in a 2-heap and returns the information
    which nodes of the heaps where altered during the operation up to the root.

    This method is needed in the Eppstein algorithm when creating the heaps H_T.
    We need to keep track of the marked nodes because only those will become
    new nodes in the path graph P(G).

    Parameters
    ----------
    heap: array(2-tuples)
        some min 2-heap, ordered by first element of tuples
    item: 2-tuple
        first element of tuple is the key used for the heap order
        second element is its value, real content of the item

    Returns
    -------
    list(bool):
        the indexes with value true are the indexes of nodes in the heap that
        were altered during the push operation. Array has same length as new heap.

    """

    # add the item to the last position of heap and mark position:
    marks = [False] * len(heap)
    heap.append(item)
    marks.append(True)
    index_current = len(heap)-1 #index of newly added element

    #reheap by swapping element with parent until parent is smaller or element arrived at root
    while index_current > 0: #while current element is not root of heap

        #mark parent as changed
        index_parent = (index_current-1)//2
        marks[index_parent] = True

        if  heap[index_current][0] < heap[index_parent][0]:

            #swap elements:
            temp = heap[index_current]
            heap[index_current] = heap[index_parent]
            heap[index_parent] = temp

            index_current = index_parent

        else:
            index_current = index_parent
            break

    # now mark all other nodes on path to root, too (even those that were not
    # swapped!) -> this gets executed if the newly inserted element wasn't
    # sifted up to the root position (or one below)
    while index_current > 0:
        index_parent = (index_current-1)//2
        marks[index_parent] = True
        index_current = index_parent

    return marks
