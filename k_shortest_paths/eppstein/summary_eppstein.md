## Eppsteins k-shortest paths algorithm

## Prerequisites
given:  Graph G = (V,E)
* with n vertices and m edges
* directed
* weighted, all edged have non-negative lengths
* start s \in V, destination t \in V
* (connected)

##Steps of the algorithms

1)  Apply Dijkstra on reversed graph (direction of edges inverted) with t as source
    in order to get the single-destination shortest path tree T with destination t.

    * reverse Graph
    * use built-in function single_source_dijkstra from networkx
    * convert dictionary of paths to an instance of tree Class

    runtime: O(m + n log n)

    further steps:
    * determine/mark edges in G-T
    * compute \delta(e) for all edges e \in G-T

2)  Build heaps H_{out}(v) for all vertices v \in V

    * resulting heaps are 2-heaps except for the root (the so called outroot) which has just one child

    * traverse graph and visit each vertex v once
    * determine out(v) for this vertex
    * build and store heap ordered by values of \delta(e) for all edges e \in out(v)


    runtime: O(M)

3)  Build 2-heaps H_T(v) for all vertices v \in V

    * traverse graph starting from t using depth first search
    (make sure to reuse results from respective parent by just adding new elements to a copy of that heap)

4)  Merge H_T(v) and H_{out}(w_i) for all neighboring nodes w_i of v to build heap H_G(v)

5)  build D(G)

6)  build P(G)

7)  breadth-first-search on P(G) to get k shortest paths

    * reconstruct explicit paths form implicit representations
