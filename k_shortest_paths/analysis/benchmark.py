from k_shortest_paths.graph_generator.generate_graphs import generate_random_graph
from k_shortest_paths.graph_generator.generate_graphs import generate_neighbourhood_graph
from k_shortest_paths.graph_generator.generate_graphs import calculate_k_shortest_paths
from k_shortest_paths.graph_generator.graph_with_shortest_paths import GraphWithShortestPaths

from k_shortest_paths.eppstein.eppstein import eppstein_k_shortest_paths
from k_shortest_paths.hershberger.hershberger import hershberger_k_shortest_paths

from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import save_graph_with_shortest_paths
from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import load_graph_with_shortest_paths_list

from k_shortest_paths.analysis.statistic import Statistic
from k_shortest_paths.analysis.timer import Timer

from k_shortest_paths.common.digraph_utils import length

from datetime import datetime
import networkx as nx
import json
import jsonpickle
from pathlib import Path
import os
from itertools import islice

STATISTICS_FOLDER_NAME = "statistics"

# List of tuples, representing the size of the neighbourhood-graphs
# where the first value is the number of rows, and the second value is the number of columns
# generated in each neighbourhood-graph.
GENERATED_NEIGHBOURHOOD_GRAPHS = [(25, 40), (50, 100), (100, 100)]

# List of number of nodes each random graph should contain.
GENERATED_RANDOM_GRAPHS = [250, 500, 1000]

def generate_benchmark_graphs():
    """Generates Graphs for running benchmarks.

    We generate two types of graphs: Neighbourhood-Graphs and Random graphs.
    Both types are DAGs and each edge inside each generated graph has random weights (attribute name is "weight").

    Each graph is an instance of GraphWithShortestPaths, because we can specify `s`- and `t`-nodes inside the these instances
    such that the benchmark-test can use these nodes to run the test.

    We hardcoded the number of graphs and the sizes of the graphs, because we thought, it would not be important for users
    to chose these numbers.

    Generated graphs are saved within the graphs folder, prefixed by 'benchmark'.
    """

    # test graphs are instances of GraphsWothShortestPaths, but the attribute `shortest_paths` is not set.

    # generate neighbourhood graphs with 1k nodes, 5k nodes and 10k nodes with random weights
    print("Generating Neighbourhood-Graphs...")
    for (n, m) in GENERATED_NEIGHBOURHOOD_GRAPHS:
        # k = 0, because we dont need to pre-calculate any shortest paths. (we have tests for this purpose)
        print("    Generating Neighbourhood-Graph with {} nodes...".format(n * m))
        G = calculate_k_shortest_paths(generate_neighbourhood_graph(n, m), 0, 1, n*m)
        print("    Generated Neighbourhood-Graph with {} nodes!".format(n * m))
        print("    Saving Neighbourhood-Graph with {} nodes...".format(n * m))
        location = save_graph_with_shortest_paths(G, n*m, 0, generated_graph_type="neighbourhood", prefix="benchmark")
        print("    Saved Neighbourhood-Graph with {} nodes at {}!".format(n * m, location))

    print("Done generating Neighbourhood-Graphs!")

    print("Generating Random DAGs...")
    # generate random connected DAGs
    for n in GENERATED_RANDOM_GRAPHS:
        # generate 10 random graphs each. afterwards list the generated graphs in ascending order
        # by their edge-count. We chose the 3 graphs in the middle, so we have a nice representation
        # of random graphs with similar edge-sizes. This results in more sensible benchmark-test.
        random_graphs_by_edges = [() for i in range(10)]

        for i in range(10):
            print("    Generating Random DAG with {} nodes...".format(n))
            G = None
            # We generate a random Graph and make sure it has a suitable s-t-path
            found_random_graph_with_path = False
            while (not found_random_graph_with_path):
                try:
                    # k = 0, because we dont need to pre-calculate any shortest paths. (we have tests for this purpose)
                    G = calculate_k_shortest_paths(generate_random_graph(n), 0, 1, n)
                    found_random_graph_with_path = True
                except nx.NetworkXNoPath:
                    pass
                    print("    ...Try generating Random DAG with {} nodes again. (There was no s-t-path in this graph)...".format(n))

            random_graphs_by_edges[i] = (len(G.edges),  G)
            print("    Generated Random DAG with {} nodes and {} edges!".format(n, len(G.edges)))

        # sort by first tuple element
        random_graphs_by_edges = sorted(random_graphs_by_edges, key=lambda tup: tup[0])


        # 3, 4 and 5 are exactly the middle three elements of a size-10 list
        for i in [3, 4, 5]:
            print("    Saving Random DAG with {} nodes and {} edges...".format(n, random_graphs_by_edges[i][0]))
            location = save_graph_with_shortest_paths(random_graphs_by_edges[i][1], n, 0, prefix="benchmark")
            print("    Saved Random DAG with {} nodes and {} edges at {}.!".format(n, random_graphs_by_edges[i][0], location))

    print("Done generating Random DAGs!")



def get_statistics_for_graphs(graphs: [], ks: [], ksp_algorithm, iterations: int) -> dict:
    """Runs the given KSP algorithm on each graph in the `graphs` list a given number of times for each `k` inside `ks`,
    measures the runtime of each execution and returns a dictionary, keyed by the size of the graph with values being a list of Statistics

    Parameters
    ----------
    graphs: list of GraphsWithShortestPaths
        List of graphs, on which we run `ksp_algorithm` on and measure the time. Each graph has to be an instance
        of GraphsWithShortestPaths, because these instances contain information on which node to start and to end.
    ks: list of int
        List of `k`'s. On each graph we will find `k` shortest paths `iterations` number of times, for each `k` in `ks`.
    kas_algorithm: k-shortest-paths-function
        Either `k_shortest_paths.hershberger.hershberger.hershberger_k_shortest_paths`, or
        `k_shortest_paths.eppstein.eppstein.eppstein_k_shortest_paths`

    Returns
    dict
        Dictionary, keyed by number of nodes inside graphs and valued by list of `k_shortest_paths.analysis.statistic.Statistic`.
        Each Statistic represents one iteration of one ksp-algorithm with a specific `k` on graphs with `n` nodes.
    """
    stats = dict({})
    for G in graphs:
        n = len(G.nodes)
        # following condition could fail, if G has no nodes, but this is a benchmark test
        # so G usually hat many nodes
        if not stats.get(n, False):
            stats[n] = []

        for k in ks:
            for i in range(iterations):
                timer = Timer()

                print("    Running {}. iteration of {} with {}-shortest-paths on graph with {} nodes...".format(i+1, ksp_algorithm.__name__, k, n))
                timer.start()
                ksp_algorithm(G, G.shortest_paths_from, G.shortest_paths_to, k)
                timer.stop()
                print("    Finished {}. iteration of {} with {}-shortest-paths in on graph with {} nodes {}ns".format(i+1, ksp_algorithm.__name__, k, n, timer.time))
                stat = Statistic(k, timer.time)
                stats[n].append(stat)

    return stats


def save_benchmark_statistics(graph_statistics: dict, timestamp: str, graphs_type: str):
    """Saves Benchmark-statistics as JSON-files inside the `statistics`-Folder.

    We chose JSON, so that the files can be read by more readers.

    Parameters
    ----------
    graph_statistics: dictionary
        each entry is usually keyed by number of nodes of the graph tested, and valued by a list of statistics.
    timestamp: str
        timestamp of when the benchmark test was ran
    graphs_type: str
        type of graphs used in the tests (either 'benchmark' or 'random')

    """

    # create folder, if not exists:
    Path(os.path.join(os.getcwd()), STATISTICS_FOLDER_NAME).mkdir(exist_ok=True)

    filename = "{}/benchmark_stats_{}_{}.json".format(STATISTICS_FOLDER_NAME, graphs_type, timestamp)
    with open(filename, 'w') as f:
        json.dump(json.loads(jsonpickle.encode(graph_statistics, unpicklable=False)), f)

def run_benchmark():
    """ Runs a benchmark test against graphs inside our graphs-folder and saves the statistics inside the `statistics` folder.

    Runs a benchmark test against graphs inside our graphs-folder, containing names "random" or "neighbourhood".
    These names were added to the graphs inside the `generate_benchmark_graphs()` method.

    We run a benchmark test on neighbourhood-graphs and random graphs.
    A test-run is executed inside `get_statistics_for_graphs`.
    Each test run is provided with a set of graphs, a list of values for `k` and the number of iterations.

    On each graph, for each `k` in the list of `k`s, we run each ksp-algorithm `iteration` number of times.

    Each time, we save the runtime of each graph (identified by its number of nodes).

    In the end, we will have run each algorithm `len(list of 'k's) * iteration * len(graphs we test against)` times.

    The statisics of these runs are collected as two dictionaries:
    One dictionary for neighbourhood graphs and one dictionary for random graphs.

    Each entry in each dictionary is keyed by the number of nodes of a graph the test has been run against.
    Each entry in each dictionary is valued by a list of `k_shortest_paths.analysis.statistic.Statistic`'s.


    Finally these dictionaries are saved inside the `statistics` folder with a time-stamp.
    """
    # now, we do the statistics
    k_neighbourhood = [5, 10, 20, 30, 50, 100]
    k_random = [5, 10, 20, 40, 60, 80]
    iterations = 10 #this should be larger than 1, otherwise the plot function will give an error

    # dict, keyed by number of nodes, values are list of Statistic
    neighbourhood_stats = dict({})
    neighbourhood_graphs = load_graph_with_shortest_paths_list("benchmark_graph_neighbourhood*")
    print("Found {} neighbourhood-graphs for benchmark-testing.".format(len(neighbourhood_graphs)))
    neighbourhood_stats["eppstein"] = get_statistics_for_graphs(neighbourhood_graphs, k_neighbourhood, eppstein_k_shortest_paths, iterations)
    neighbourhood_stats["hershberger"] = get_statistics_for_graphs(neighbourhood_graphs, k_neighbourhood, hershberger_k_shortest_paths, iterations)
    neighbourhood_stats["networkx"] = get_statistics_for_graphs(neighbourhood_graphs, k_neighbourhood, __networkx_k_shortest_paths, iterations)
    print("Finished testing neighbourhood-graphs!")

    # dict, keyed by number of nodes, values are list of Statistic
    random_stats = dict({})
    random_graphs = load_graph_with_shortest_paths_list("benchmark_graph_random*")

    print("Found {} random-graphs for benchmark-testing.".format(len(random_graphs)))
    random_stats["eppstein"] = get_statistics_for_graphs(random_graphs, k_random, eppstein_k_shortest_paths, iterations)
    random_stats["hershberger"] = get_statistics_for_graphs(random_graphs, k_random, hershberger_k_shortest_paths, iterations)
    random_stats["networkx"] = get_statistics_for_graphs(random_graphs, k_random, __networkx_k_shortest_paths, iterations)
    print("Finished testing random-graphs!")

    timestamp = int(datetime.now().timestamp())
    print("Saving results...")
    save_benchmark_statistics(neighbourhood_stats, timestamp, 'neighbourhood')
    save_benchmark_statistics(random_stats, timestamp, 'random')


    print("Results have been saved.")
    print("Benchmark test has finished.")

def __networkx_k_shortest_paths(G, source, target, k):
    """Short method for calculating K shortest simple paths with the networkx library.

    Parameters
    ----------
    G: nx.DiGraph
        Graph, which we compute the shortest s-t-paths on
    s: node
        source-node
    t: node
        target-node
    k: number
        number of shortest paths to compute

    Returns
    -------
    list of tuple
        A list of tuples containing `K` paths paired with their lengths.
        A path is a list of nodes. The first entry of the tuple is the path, the second is the length of the path.
    """
    paths = list(islice(nx.shortest_simple_paths(G, source, target, weight="weight"), k))
    lengths = (length(G, path) for path in paths)
    return list(zip(paths, lengths))
