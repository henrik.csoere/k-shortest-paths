"""This package contains modules for analyzing our algorithms.

It contains a Timer for measuring the runtime of our algorithms and a module for executing benchmark-tests.
"""