import time

class Timer():
    """Instances of this class measure times.

    If you want to start measuring, use an insance of this class and call `start()`.
    To stop, use `stop()`. The measured time is being returned by the stop method, or can be accessed 
    after stopping with the `time` attribute.
    """

    def __init__(self):
        self.tik = 0.0
        self.tok = 0.0
        self.time = 0.0
        

    def start(self):
        """Starts the timer.

        To stop the timer, call `stop()`
        """
        self.tik = time.perf_counter_ns()

    def stop(self):
        """Stops the timer and returns the time measured.

        Returns
        -------
        time: int
            Time passed between `start()` and `stop()`.
        """
        self.tok = time.perf_counter_ns()
        self.time = self.tok - self.tik

        return self.time
