from networkx import DiGraph
import networkx as nx
#import my_dijkstra as md



def dijkstra(G: DiGraph, s, t, weight="weight"):
    """Algorithm for finding the shortest path from `s` to `t`
    in `G` where `weight` denotes the weight
    of the edges in `G`. `weight` is a string, which denotes
    the attribute of an edge containing the weight.

    Returns a list of nodes of the shortest path and the length of the path
    as a tuple (first entry is a list of nodes and second entry is length).
    """

    return (dijkstra_shortest_path(G, s, t, weight), dijkstra_shortest_path_length(G, s, t, weight))

def dijkstra_shortest_path(G: DiGraph, s, t, weight="weight"):
    """Algorithm for finding the shortest path from `s` to `t`
    in `G` where `weight` denotes the weight
    of the edges in `G`. `weight` is a string, which denotes
    the attribute of an edge containing the weight.

    Returns a list of nodes of the shortest path.
    """

    return nx.dijkstra_path(G, s, t, weight)

def dijkstra_shortest_path_length(G: DiGraph, s, t, weight="weight"):
    """Algorithm for finding the shortest path length from `s` to `t`
    in `G` where `weight` denotes the weight
    of the edges in `G`. `weight` is a string, which denotes
    the attribute of an edge containing the weight.

    Returns the lenght of the shortest path.
    """

    return nx.dijkstra_path_length(G, s, t, weight)



def dijkstra_single_source(G: DiGraph, s, weight="weight"):
    """Algorithm for finding the shortest path from source `s`
    to all other reachable nodes in `G` where `weight` denotes
    the weight of the edges in `G`.
    `weight` is a string, which denotes
    the attribute of an edge containing the weight.

    Returns a tuple of two dicts, the first containing the distances
    of the shortest path to each target node, the second the shortest
    path to each target node,.

    """

    #return md.dijkstra(G, s)
    return nx.single_source_dijkstra(G, s, weight=weight) # from networkx
