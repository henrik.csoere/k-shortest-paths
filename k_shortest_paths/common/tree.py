from networkx import DiGraph

class Tree(DiGraph):
    """Instances of this class are connected, acyclic ``networkx.DiGraph``s 
    with a designated vertex `root`.
    In other words, instances of this class are trees 
    with a designated root called `root`.
    
    Important: We assume this tree is not root-directed, 
    which means every node is directed to its children.
    
    We created this class especially for convenience and readability, 
    as we renamed methods for  Tree-specific tasks, 
    e.g. ``DiGraph.successors(v)`` is renamed as
    `get_children(v)` or ``DiGraph.predecessors(v)`` 
    is used in `get_parent(v)` for getting the parent of `v`.
    """

    def __init__(self, root = None, T = DiGraph(), paths_from_root = {}, distances_from_root = {}):
        """Instanciates a tree with root `root`. If `root` is `None`, 
        an empty `networkx.Digraph` is created. Also, optionally takes a Graph `T` which attributes 
        will then be copied to this instance of Tree.

        If `graph` is empty (has no vertices), the value of `root` is ignored and
        set to `None`. 

        Parameters
        ----------
        root: any
            (optional) Designated root-node of the Tree. 
        T: DiGraph
            (optional) Graph which the Tree should be initialized with. We do not check, whether `T` is actually a tree.
            The caller is responsible for doing so.
        paths_from_root: dict: {node: list of nodes}
            (optional) If `T` is omitted, this parameter will be ignored.
            Resembles the unique paths from the root-node to any other node in this tree.
            This dict is not generated. The caller has to provide it.
        distances_from_root: dict: {node: number}
            (optional) If `T` is omitted, this parameter will be ignored.
            For each node `v` in `T` this dict stores a number, which represents the distance from `root` to `v`.
            Note, that `distances_from_root` do not have to necessarily be the sum of weights of edges from `root` to any other node.
            This dictionary is for example computed and used in the Hershberger-Algorithm, in order to be able to calculate path-lengths efficiently.
            This dict is not generated. The caller has to provide it.
        """
        super().__init__(T)

        self.paths_from_root = dict()

        if T is not None: 
            self.paths_from_root = paths_from_root
            self.distances_from_root = distances_from_root

        if root is not None:
            self.add_node(root)
        
        self.root = root


    def get_parent(self, v):
        """ returns the parent of the vertex `v` in this tree,
        or ``None`` if `v` is the root. """
        if (len(list(self.predecessors(v))) == 0):
            return None
        return list(self.predecessors(v))[0]

    def get_children(self, v):
        """ returns an iterable of vertecies of the children of `v`. """
        return self.successors(v)


