import math
import networkx as nx
from networkx import DiGraph
import heapq

def initialize_distances(G, s):
    """ Creates an empty dictionary for storing the distances of the shortest
    paths from s to every other vertex.

    At first all distances to every node from starting node s are positive
    infinity and no node is visited.
    Distance infinity means therefore that no path starting from s to the
    respective node has been found
    """
    distances = {v: math.inf for v in G.nodes}
    distances[s] = 0
    return distances


def initialize_shortest_paths(G, s):
    """Creates an empty dictionary for storing the shortest paths from s to
    every other vertex.

    At first all paths are the empty path except for node s that can be reached
    visiting only s so the shortest path to s is the path only consisting of
    node s.
    """
    shortest_paths = {v : [] for v in G.nodes()}
    shortest_paths[s].append(s)
    return shortest_paths


def initialize_neighbors(G, s, weight):
    """Finds the neighbors of the start node and returns them in a priority
    ordered by their distance to `s`

    Parameters
    ----------
    G: nx.DiGraph
        A weighted directed graph
    s: node
        start node
    weight: str
        The name of the attribute of G's edges specifying their weight.

    Returns
    -------
    list of 2-tuples
        A min heap (priority queue) that contains the neighbors of `s`.
        The first element of a tuple is the weight of the edge from `s` to
        a neighbor n, the second element is the neighbor node n.
        The first element in this heap is hence the closest neighbor.
    """
    weighted_initial_neighbors = [(0, s)]#G.successors(s)
    #weighted_initial_neighbors = [(G.edges[s,n][weight], n) for
    #                                n in initial_neighbors]
    #heapq.heapify(weighted_initial_neighbors)
    return weighted_initial_neighbors


def set_current_node(current_neighbors):
    """Set the non-visited node with the smallest
    current distance to the start node as the current node.
    Returns None if there is no more node to visit.
    """

    # visited is a set that contains the names of the nodes marked as visited. E.g. {'A', 'C'}.
    # currentDistances is a dictionary that contains the current minimum distance of each node. E.g. {'A': 0, 'B': 3, 'C': 5}
    if current_neighbors:
        closest_neighbor = heapq.heappop(current_neighbors)
        new_current_node = closest_neighbor[1]
        return new_current_node
    return None


def update_distances(G: DiGraph, current_node, visited, distances, shortest_paths, current_neighbors, weight="weight"):
    """Examines all successors of the current node c and updates the distances
    of the start node to these sucessors and the corresponding paths (that can
    contain c then)
    """
    for v in G.successors(current_node):

        # check if current_node's successors can be reached more quickly
        # by passing node current_node compared to the so far known shortest
        # paths to them

        if v in visited:
            continue # new path cannot be shorter

        new_distance = distances[current_node] + G.edges[current_node, v][weight]
        if (new_distance < distances[v]):
            distances[v] = new_distance
            shortest_paths[v] = shortest_paths[current_node].copy()
            shortest_paths[v].append(v)

        if v in current_neighbors:
            # decrease key operation (not directly supported by heapq module):
            dec_key(current_neighbors, v, new_distance)
        else:
            heapq.heappush(current_neighbors, (new_distance,v) )


def dec_key(heap, element, new_priority):
    """Decrease key operation in a min heap
    """
    # first find the element in the heap, more specifically:
    # find the index of the element in the heap
    elements = [tuple[1] for tuple in heap]

    if element not in heap:
        return

    i = elements.index(element)

    # update priority
    if (new_priority > heap[i][0]):
        heap[i][0] = new_priority

    # reheap by swapping element with parent until parent is smaller or
    # element arrived at root
    while i > 0: # is current element already root of heap?

        index_parent = (i-1)//2

        if  heap[i][0] < heap[index_parent][0]:

            #swap elements:
            temp = heap[i]
            heap[i] = heap[index_parent]
            heap[index_parent] = temp

            i = index_parent

        else:
            return


def dijkstra_pq(G: DiGraph, s, weight="weight"):
    """Implementation of single source Dijkstra algorithm for a directed graph.

    Computes the shortest paths in directed graph G from node s to all other
    nodes using priority queues.

    Multiple edges between nodes are not allowed.

    Parameters
    ----------
    G: nx.DiGraph
        A weighted directed graph
    s: node
        start node
    weight: str
        The name of the attribute of G's edges specifying their weight.


    Returns
    -------
    distances : dict [node, int]

    shortest_paths: dict [node, list [node]]
        {vertex_1: [s, ... , vertex_1],..., vertex_n : [s, ... , vertex_n]}
    where [s, ... , vertex_i] is the ordered list of nodes forming the path from 's' to vertex_i
    distances: a dict of the of the format {vertex_1: dist_1, ... vertex_n: dist_n} where dist_i is the
    distance from 's' to vertex_i, i.e. the length of the shortest path between those nodes.

    """

    visited = []
    distances = initialize_distances(G, s)
    shortest_paths = initialize_shortest_paths(G, s)
    current_neighbors = initialize_neighbors(G, s, weight) # priority queue (min heap)

    while current_neighbors:
        current_node = set_current_node(current_neighbors)
        visited.append(current_node)
        update_distances(G, current_node, visited, distances, shortest_paths, current_neighbors, weight)


    return distances, shortest_paths
