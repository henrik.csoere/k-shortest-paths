"""
    Saves and loads generated DAGs from the local directory.
    These DAGs are saved as binary .pickle files.

"""

import glob
import os
from k_shortest_paths.graph_generator.graph_with_shortest_paths import GraphWithShortestPaths
import pickle
from pathlib import Path
import sys
from k_shortest_paths import graph_generator

GRAPH_DIRECTORY_NAME = "graphs/"

def load_graph_with_shortest_paths_list(name="*"):
    """Load graphs inside the graphs folder from a pickle file matching `name`.

    Returns a list of instances of GraphsWithShortestPaths from the `graphs`-Folder inside 
    the root-directory of our project-folder.

    The caller my specify a pattern to match the file-names. We use `glob` for that. 
    
    Here's the documentation from `glob` for reference on how to use the patterns:
    "The pattern may contain simple shell-style wildcards a la fnmatch. 
    However, unlike fnmatch, filenames starting with a dot are special cases that are not matched by '*' and '?' patterns."

    Parameters
    ----------
    name: str
        (optional, default = "*") file-name-pattern, always suffixed by `.pickle`. 
        The pattern may contain simple shell-style wildcards a la fnmatch. 
        However, unlike fnmatch, filenames starting with a dot are special cases that are not matched by '*' and '?' patterns.


    Returns
    -------
    List of GraphsWithShortestPaths
        list of GraphsWithShortestPaths inside the `graphs`-folder matching the pattern.
    """
    graphs = []
    sys.modules['graph_generator'] = graph_generator
    for filename in glob.glob(os.path.join(GRAPH_DIRECTORY_NAME, '{}.pickle'.format(name))):
        with open(filename, 'rb') as f:
            graph = pickle.load(f)
            graphs.append(graph)

    return graphs


def save_graph_with_shortest_paths(graph_with_shortest_paths: GraphWithShortestPaths, n, k, generated_graph_type = "random", prefix="test"): 
    """Saves the given graph as a pickle file with a generic name, specified by the parameters.

    Serialized the Graph, so later one does not have to generate the graphs again, but can easily read the graphs
    with `load_graph_with_shortest_paths_list`

    Parameters
    ----------
    graph_with_shortest_paths: GraphWithShortestPaths
        Graph, which will be saved
    n: int
        Information of how many nodes the graph has, which is put into the filename.
    k: int
        Information of how many shortest-paths have been calculated on the graph, which is put into the filename
    generated_graph_type: str
        Information of which the the saved graph is. (Either 'random' or 'neighbourhood')
    prefix: str
        Prefix in the name of the file (usually 'test' or 'benchmark')
    
    Returns
    -------
    str
        filename of the saved graph.
    """

    # create folder, if not exists:
    Path(os.path.join(os.getcwd()), GRAPH_DIRECTORY_NAME).mkdir(exist_ok=True)

    filename = GRAPH_DIRECTORY_NAME + "{}_graph_{}_{}-nodes_{}-paths.pickle".format(prefix, generated_graph_type, n, k)
    
    j = 0
    while Path(filename).is_file():
        j += 1
        filename = GRAPH_DIRECTORY_NAME + "{}_graph_{}_{}-nodes_{}-paths({}).pickle".format(prefix, generated_graph_type, n, k, j)

    with open(filename, 'wb') as f:
        pickle.dump(graph_with_shortest_paths, f)

    return filename


def check_if_serialized_graphs_exist(name = None):
    """Checks if the Folder containing test and benchmark graphs exists and if the pattern `name` is specified, 
    checks whether there are graphs like `name`. 
    
    Parameters:
    name: string
        (optional) Name of desired files inside the graphs-folder. If specified, method will return True, iff there are any
        files matching the name inside the graphs-folder, given the graphs-folder exists.
        Name can be a pattern. The pattern may contain simple shell-style wildcards a la fnmatch. (default: None)

    
    Returns
    -------
    boolean
        True, if folder exists and `name` is None, or if folder exists and there exists a file matching `name`.
    """

    if name is None:
        return os.path.isdir(os.path.join(os.getcwd()), GRAPH_DIRECTORY_NAME)

    return len(glob.glob(os.path.join(GRAPH_DIRECTORY_NAME, '{}'.format(name)))) > 0