class PathBranchStructureNode:
    """ Instances of this class represent a Node of the Path Branch Structure 
    of Hershberger's k-shortest-paths algorithm (see hershberger.py for more details).

    We define this class to bundle the attributes a node in `T` needs, in order to be
    respresented correctly in `T`.

    Some nodes of `G` may be added to `T` up to `k` times. For example, in `T``
    there are always `i` different leaves `t`, for `i = 1, 2, ..., k` during contruction.

    Therefore each node in `T` has to have a unique identification, 
    such that each leave `t_i` can be added into `T`, without overriding existing 
    `t_j`s. We use `u_id` ("unique.ID"), which is a static attribute of this class, to 
    identify each node uniquely.
    Still, we need to have information about the original node from `G`. We let `name` be 
    the attribute, which stores the original name of the node of `G`.

    Finally, in our Path Branch Structure `T`, every Node `u` has a coresponding
    prefix_path, which is the concatenation of branch_paths from the root `s` up to `u`.
    We define a coresponding attribute `prefix_path`, in which the caller 
    has to make sure to put the correct prefix_path into.
    """
    # unique ID as class-attribute
    current_u_id = 0

    def __init__(self, name, prefix_path):
        self.u_id = PathBranchStructureNode.current_u_id
        self.name = name
        self.prefix_path = prefix_path
        PathBranchStructureNode.current_u_id = PathBranchStructureNode.current_u_id + 1

    def __hash__(self):
        return self.u_id
    
    def __repr__(self):
        return "<u_id: {}, name: {}, prefix_path:{}>".format(self.u_id, self.name, self.prefix_path)

    def __str__(self):
        return self.__repr__()