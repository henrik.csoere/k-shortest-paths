import networkx as nx
import k_shortest_paths.common.dijkstra as dijkstra
import math
from k_shortest_paths.common.tree import Tree
import heapq as heapq
from k_shortest_paths.common.digraph_utils import single_source_shortest_path_tree_T

def best_replacement_path(H: nx.DiGraph, replacements: [], x, y, weight="weight"):
    """We compute the shortest replacement path in `H` from `x` to `y`, by removing
    exactly one of the edges (v_1, v_2), (v_2, v_3), ..., 
    with v_ i being inside `replacements` and then computing an `x`-`y`-path not using at least ohne of these edges.
    
    We implement Hershberger and Suri's algorithm for finding such a path.
    In this algorithm we look at the shortest path trees `X` with `x` as root
    and `Y` with `y` as root. By removing any edge `(a, c)` of 
    `P` := shortest path from `x` to `y`, starting from the edge which starts in `x`, we cut `X` into two components. 
    Let `E_{a, c}` be the set of edges `(a, b)` of `H` where `a` and `b` each lie in different components of `X`
    
    The shortest path without `(a, c)` is 
    `P_{a,c}` := `x = v_1`, `v_2`, ... `a`, `b`, `v_i`, ..., `v_l = y` where 
    `d(P_{a,c})` is the smallest for all `(a, b)` in `E_{a, c}`.

    The smallest of these paths is returned.

    Parameters
    ----------
    H: DiGraph
        The Original DiGraph, which we want to get the best replacement path for
    replacements: list of nodes
        nodes, which form a path from `x`, but not neccessarily to `y`, as
        the list of nodes could represent only a subpath of `P`. The first node should always be `x`.
    x: node
        source-node
    y: node
        destination-node


    Returns
    -------
    tuple of (list of nodes, length)
        A tuple, containing the shortest path in `H` from `x` to `y` not using at least one
        of the edges in `replacements` paired with its length.
    
    Notes
    -----
    This algorithm is known to fail in some specific cases. 
    Therefore an exception mechanism must be implemented, which then 
    uses the slower but correct algorithm for finding the best replacement path.

    References
    ----------
    See http://www.eecs.harvard.edu/~parkes/cs286r/spring02/papers/vickrey.pdf

    """

    shortest_replacement_path_length = math.inf
    shortest_replacement_path = []
    
    # if there are no edges to replace, we cannot calculate any paths.
    # raise a NoPaths-Error.
    if len(replacements) < 2:
        raise nx.NetworkXNoPath

    # Y is used for detecting invalid edges, for which we have to call the naive replacement path algorithm.
    X, Y = __calculate_shortest_path_trees(H, x, y, weight)

    replacement_edges = [ (replacements[i], replacements[i+1]) for i in range(len(replacements) - 1) ]

    # The shortest path from `x` to `y` is contained in the SPT in X and has already been calculated.
    shortest_xy_path = X.paths_from_root[y]
    shortest_xy_path_edges = [ (shortest_xy_path[i], shortest_xy_path[i + 1]) for i in range(len(shortest_xy_path) - 1) ]

    # L and R both contain len(shortest_xy_path) sets of edges

    # Let e_i := (v_i, v_i+1) := (a, b) for each edge in P.
    # Let u be a node of X (or H, or Y. Doesn't matter since they are all the same nodes). 
    # Define block(u) as the maximum index i of the vertex v_i in P, where v_i is a successor of u.
    # An edge e_i = (a, b) belongs to the cut set E_i, iff block(a) <= i < block(b)

    # Let R[block[b]] = L[block[a]] contain edges (a, b) of E \ X with block(a) < block(b)
    # which means each edge (a, b) is associated with the path vertices where it enters and leaves E_i
    L = [ [] for x in range(len(shortest_xy_path) + 1) ]
    R = [ [] for x in range(len(shortest_xy_path) + 1) ]

    for (a, b) in H.edges:
        if X.has_edge(a, b) or not X.has_node(a) or not X.has_node(b):
            continue

        if H.nodes[a]['block'] < H.nodes[b]['block']:
            L[H.nodes[a]['block']].append((a, b))
            R[H.nodes[b]['block']].append((a, b))

    # initialize the priotity-queue
    Q = []

    for i in range(1, len(replacement_edges) + 1):
        current_replacement_path_length = math.inf
        current_replacement_path = []
        # step (a)
        for (u, v) in L[i]:
            # It may be the case, that there is no path from v to y. 
            # a KeyError will be thrown, because v does not exist in Y
            try:
                x1 = X.distances_from_root[u]
                uv1 = H[u][v][weight]
                y1 = Y.distances_from_root[v]
                path_weight = X.distances_from_root[u] + H[u][v][weight] + Y.distances_from_root[v]
                heapq.heappush(Q, (path_weight, (u, v)))
            except KeyError:
                pass
        
        # step (b)
        for e in R[i]:
            # TODO: Do more efficiently, by maybe using a map, 
            # keyed by edges and valued by a reference to an entry of Q
            for (w, _e) in Q:
                if e == _e:
                    Q.remove((w, _e))
                    heapq.heapify(Q)
        
        # step (c)
        # check if the min-edge is valid.
        if len(Q) > 0:
            (current_replacement_path_length, (u, v)) = Q[0]

            if H.nodes[v]['low'] <= i:
                # revert to slower computation of shortest path not using edge e_i = (v_i, v_i + 1)
                (current_replacement_path, current_replacement_path_length) = __best_replacement_path_naive(H, replacement_edges[i - 1], x, y)
            else:
                p_x = X.paths_from_root.get(u)[:-1]
                p_y = Y.paths_from_root.get(v)[:]
                p_y.reverse()
                p_y = p_y[1:]
                current_replacement_path = p_x + [u, v] + p_y

            if (current_replacement_path_length < shortest_replacement_path_length):
                shortest_replacement_path_length = current_replacement_path_length
                shortest_replacement_path = current_replacement_path
        
    if (shortest_replacement_path_length == math.inf):
        raise nx.NetworkXNoPath
    
    return (shortest_replacement_path, shortest_replacement_path_length)



def __best_replacement_path_naive(H: nx.DiGraph, replacement_edge, x, y, weight="weight"):
    """We compute the shortest path in `H` from `x` to `y`, but 
    each time without using one of the edges in `P`.
    
    We use Dijkstra's Algorithm to compute the shortest path from `x` to `y`
    for each subgraph `H_e`, of which one edge `e` in `P` 
    is removed from `H`.

    Parameters
    ----------
    H: DiGraph
        The Original DiGraph, which we want to get the best replacement Path for
    replacement_edge: edge
        edge which should not be used while finding the shortest path. represented as a tuple of nodes of `H`.
    x: node
        source-node
    y: node
        target-node

    Returns
    -------
    tuple of (list of nodes, length)
        A tuple, containing the shortest path in `H` from `x` to `y` not using at least one
        of the edges in `P` and its length.
    """
    if replacement_edge not in H.edges:
        raise nx.NetworkXNoPath 

    (u, v) = replacement_edge

    # save the weight for later, when reattaching the edge again.
    w = H.get_edge_data(u, v)[weight]
    H.remove_edge(u, v)
    replaced_path = []
    replaced_path_length = -math.inf

    try: 
        # here it's possible that there is no path between x and y after removing the edge.
        replaced_path = dijkstra.dijkstra_shortest_path(H, x, y)
        replaced_path_length = dijkstra.dijkstra_shortest_path_length(H, x, y)
    finally:
        H.add_edge(u, v, weight=w)

    return (replaced_path, replaced_path_length)


def __calculate_shortest_path_trees(H: nx.DiGraph, x, y, weight = "weight"):
    """Calculates the SPTs X and Y of `H`, and sets `block` and `low` labels to nodes in `H`.

    The SPT `X` starts in node `x` and SPT `Y` starts in node `y` and uses the reversed graph of `H`.

    Parameters
    ----------
    H: nx.DiGraph
        Graph, of which we calculate SPTs X and Y. In H, labels `block` and `low` are set.
    x: node
        root of SPT `X`
    y: node
        root of SPT `Y`
    weight: string
        (optional) label, which corresponds to edge-weights (default: "weight")

    Returns
    -------
    tuple of (Tree, Tree)
        Tuple (X, Y) with SPTs X and Y 
    """

    X, dx = single_source_shortest_path_tree_T(H, x, weight)
    Y, dy = single_source_shortest_path_tree_T(H.reverse(False), y, weight)

    for (block, v) in enumerate(X.paths_from_root[y]):
            nx.set_node_attributes(H, {v: {'block': block + 1 }} )

    __calculate_block_labels(H, X, x, H.nodes[x]['block'])
    __calculate_low_labels(H, X, Y, y, H.nodes[y]['block'])

    return X, Y

def __calculate_block_labels(H: nx.DiGraph, X: Tree, v, block: int):
    """Calculates the `block`-labes in `X` recursively by doing a pre-order traversal of X.
    
    `X` is an SPT of a graph `H` with root `x`.

    What is `block`?
    In `X`, there is a path `P` = `x = v_1, v_2, ..., v_n = y` going from `x` to `y`, which is the shortest `x-y` path in `H`.
    
    A node `v` in `X` has `block(v) = i`, if `v` is reached by a path `v_i, w_1, w_2, ... v` 
    starting from `v_i` and each `w_j` is not a node `v_k` of `P`. 

    Parameters
    ----------
    X: Tree
        SPT of a graph G, of which we want to calculate the `block`-attributes for each node in `X`.
    v: node
        current node, which we set `block` for. 
    block: int
        current block number, which is set for node `v`
    """
    nx.set_node_attributes(H, {v: block}, 'block')
    for child in X.get_children(v):
        if H.nodes[child].get('block', 0) > 0:
            __calculate_block_labels(H, X, child, H.nodes[child]['block'])
            continue
        __calculate_block_labels(H, X, child, block)

def __calculate_low_labels(H: nx.DiGraph, X: Tree, Y: Tree, v, low: int):
    """Calculates the `low`-labes in `Y` recursively by doing a pre-order traversal of Y.
    
    `Y` is an SPT of a graph `H` with root `y`.

    What is `low`?
    In `X`, we already have calculated all the `block`-labels.
    Let `(u, v)` be an edge in Y. `low(u) = min(block(u), low(v)`.

    Parameters
    ----------
    X: Tree
        SPT of a graph G, of which calculated the `block`-attributes for each node in `X`.
    Y: Tree
        SPT of a graph G.reversed(), of we calculate the `low`-attributes for each node in `Y`.
    v: node
        current node, which we set `low` for. 
    low: int
        current low-label, which is set for node `v`
    """
    nx.set_node_attributes(H, {v: low}, 'low')
    for child in Y.get_children(v):
        __calculate_low_labels(H, X, Y, child, min([H.nodes[child].get('block', -math.inf), H.nodes[v]['low']]))
