""" 
This module defines fixtures used by several tests.
Specifically, we define a set of different graphs 
and use them for testing each algorithm.
The methods will return a Graph and the coresponding 
shortest paths. We get the shortest paths by using `networkx.shortest_simple_paths`.
"""

import pytest
import networkx as nx
from itertools import islice
from k_shortest_paths.common.digraph_utils import length
from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import load_graph_with_shortest_paths_list
from k_shortest_paths.graph_generator.graph_with_shortest_paths import GraphWithShortestPaths
import glob

@pytest.fixture(scope="session")
def graph_with_two_paths() -> GraphWithShortestPaths:
    """This is a graph is merged from two paths from `0` to `1`."""
    G = nx.DiGraph()
    G.add_edge(0, 2, weight=2)
    G.add_edge(2, 3, weight=2)
    G.add_edge(3, 4, weight=10)
    G.add_edge(4, 1, weight=1)
    G.add_edge(0, 10, weight=1)
    G.add_edge(10, 11, weight=2)
    G.add_edge(11, 12, weight=1)
    G.add_edge(12, 1, weight=1)

    shortest_paths = [([0, 10, 11, 12, 1], 5), ([0, 2, 3, 4, 1], 15)]

    return GraphWithShortestPaths(G, 0, 1, shortest_simple_paths=shortest_paths)


@pytest.fixture(scope="session")
def graph_with_one_edge() -> GraphWithShortestPaths:
    """This is just a graph with two nodes connected by an edge."""
    G = nx.DiGraph()
    G.add_edge(0, 1, weight=1)
    return GraphWithShortestPaths(G, 0, 1, [([0, 1], 1)])

@pytest.fixture(scope="session")
def complete_bipartite_graph_k_2_100() -> GraphWithShortestPaths:
    """
    Graph which has a large 'inner' layer of nodes, each of which is connected to
    the source and the target node only. Basically, it's a complete bipartite graph K_{2, 100}, where
    the source and target nodes are in the 2-Partition.
    """
    G = nx.DiGraph()
    for i in range(2, 101):
        G.add_edge(0, i, weight=i)
        G.add_edge(i, 1, weight=i)

    return GraphWithShortestPaths(G, 0, 1, shortest_simple_paths=get_100_shortest_simple_paths(G, 0, 1))

@pytest.fixture(scope="session")
def hershberger_replacement_fail_graph() -> GraphWithShortestPaths:
    """
    This graph is contains special case for Hershberger's k-shortest-paths algorithm.
    See Figure 4 in Hershberger's k-shortest-path paper.
    """
    G = nx.DiGraph()
    G.add_edge(0, 1, weight=1)
    G.add_edge(1, 2, weight=1)
    G.add_edge(1, 3, weight=20)
    G.add_edge(2, 4, weight=1)
    G.add_edge(4, 3, weight=5)
    G.add_edge(3, 2, weight=5)
    G.add_edge(3, 5, weight=100)
    G.add_edge(4, 5, weight=1)

    return GraphWithShortestPaths(G, 0, 5, shortest_simple_paths=get_100_shortest_simple_paths(G, 0, 5))

@pytest.fixture(scope="session")
def graph_with_non_simple_paths() -> GraphWithShortestPaths:
    """
    This graph is contains a cycle and will have a list of paths, which are not simple!
    It is Hershbergers replacement fail graph, but with k=6 shortest paths calculated.
    This graph is only for Eppstein's algorithm.
    """
    G = nx.DiGraph()
    G.add_edge(0, 1, weight=1)
    G.add_edge(1, 2, weight=1)
    G.add_edge(1, 3, weight=20)
    G.add_edge(2, 4, weight=1)
    G.add_edge(4, 3, weight=5)
    G.add_edge(3, 2, weight=5)
    G.add_edge(3, 5, weight=100)
    G.add_edge(4, 5, weight=1)

    shortest_paths=[
        ([0, 1, 2, 4, 5], 4),
        ([0, 1, 2, 4, 3, 2, 4, 5], 15),
        ([0, 1, 2, 4, 3, 2, 4, 3, 2, 4, 5], 26),
        ([0, 1, 3, 2, 4, 5], 28),
        ([0, 1, 2, 4, 3, 2, 4, 3, 2, 2, 4, 3, 2, 4, 5], 37),
        ([0, 1, 3, 2, 4, 3, 2, 4, 5], 39),
    ]

    return GraphWithShortestPaths(G, 0, 5, shortest_simple_paths=shortest_paths)

@pytest.fixture(scope="session")
def hershberger_example_graph() -> GraphWithShortestPaths:
    """
    This graph is graph is given as an example in Hershberger's k-shortest-path paper.
    See Figure 2 in Hershberger's k-shortest-path paper.
    """
    G = nx.DiGraph()
    G.add_edge(0, 2, weight=1)
    G.add_edge(2, 3, weight=1)
    G.add_edge(3, 4, weight=1)
    G.add_edge(4, 5, weight=1)
    G.add_edge(5, 1, weight=1)
    G.add_edge(3, 6, weight=10)
    G.add_edge(6, 5, weight=5)
    G.add_edge(2, 7, weight=100)
    G.add_edge(7, 6, weight=20)
    G.add_edge(7, 4, weight=5)
    return GraphWithShortestPaths(G, 0, 1, shortest_simple_paths=get_100_shortest_simple_paths(G, 0, 1))

@pytest.fixture(scope="session")
def grid_graph() -> GraphWithShortestPaths:
    """
    This is a graph, which froms a small 4x4-grid, starting from the top left with the source node.
    Each node is connected to it's right and lower neighbours, if they exist 
    (such that node `1` has no outgoing edges and is in the lower right of the grid).
    The weights are contained in a matrix and are added to each edge `(i, j)` accordingly.
    """

    # size of the n-by-n grid
    n = 4
    G = nx.DiGraph()

    # matrix of weights of edges, going from left, to right.
    weights_left_to_right = [
        [4, 3, 1],
        [1, 6, 1],
        [1, 2, 1]
    ]

    # matrix of weights of edges, going from top, to bottom.
    weights_top_to_bottom = [
        [4, 3, 1],
        [1, 5, 1],
        [8, 5, 1]
    ]
    for i in range(n):
        for j in range(n):
            current_node = i * n + j
            right_neighbour = current_node + 1
            lower_neighbour = current_node + n
            # right edge
            G.add_edge(current_node, right_neighbour, weight=weights_left_to_right[i][j])
            # lower edge
            G.add_edge(current_node, lower_neighbour, weight=weights_top_to_bottom[i][j])

    return GraphWithShortestPaths(G, 0, 1, shortest_simple_paths=get_100_shortest_simple_paths(G, 0, 1))

 
@pytest.fixture(scope="session")
def graphs_inside_data_folder() -> GraphWithShortestPaths:
    """
    Here, every Graph saved as a `.pickle-File` starting with "test" inside the /graphs/ folder is tested against.
    """
    return load_graph_with_shortest_paths_list("test*")


def get_100_shortest_simple_paths(G: nx.DiGraph, s, t) -> []:
    """
    This method returns a list of the 100 shortest simple `s`-`t`-Paths in `G`
    using `nx.shortest_simple_paths()`. 
    The list consists of tuples. 
    A tuple constists of an `s`-`t`-path and its length.
    A path is a list of nodes.
    """
    paths = list(islice(nx.shortest_simple_paths(G, s, t, weight="weight"), 100))
    lengths = (length(G, path) for path in paths)
    return list(zip(paths, lengths))






