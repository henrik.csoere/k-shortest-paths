# execute in root directory with "python -m tests.test_plot_graph_path"

from networkx import DiGraph
import networkx as nx
import pandas as pd
import importlib
import k_shortest_paths.common.my_dijkstra as md
import k_shortest_paths.visualization.graph_plot as visual
from k_shortest_paths.common.digraph_utils import length
from itertools import islice
from bokeh.palettes import Reds
from k_shortest_paths.graph_generator.graph_with_shortest_paths import GraphWithShortestPaths
from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import load_graph_with_shortest_paths_list
import pandas as pd

# Create test graphs
df = pd.DataFrame({'from':['A', 'B', 'C','A', 'C', 'E', 'D'],
                   'to':['D', 'A', 'E','C', 'D', 'A','B'],
                   'weight':[8, 10, 5, 5, 2, 7, 4]})
Gpd = nx.from_pandas_edgelist(df, 'from', 'to', edge_attr = 'weight', create_using=nx.DiGraph())
G = nx.desargues_graph()

# Create test paths
shortest_path = [1,2,3,4,9]
second_path = [1,16,17,18]
third_path = [1,2,11,12,13,14,15]
pd_path = ['A','C','D']


# Plot data
#draw_graph_highlighted_path(Gpd, pd_path)
#visual.draw_graph_highlighted_path(G, shortest_path, nx.spring_layout)
#visual.draw_graph_k_paths(G, [shortest_path, second_path, third_path], nx.spring_layout)

d, sp = md.dijkstra(Gpd,'A')
print([sp[v] for v in list(Gpd.nodes)])
#visual.draw_graph_k_paths(Gpd, [sp[v] for v in list(Gpd.nodes)], nx.spring_layout)

# G_rand = grg.generate_simple_graph(10,100)
# d_rand, sp_rand = md.dijkstra(G_rand,0)
# paths_G_rand = [sp_rand[i] for i in range(1, len(G_rand))]
# visual.draw_graph_k_paths(G_rand, [sp_rand[23],sp_rand[22]], nx.circular_layout)
# visual.draw_graph_k_paths(G_rand, paths_G_rand, nx.circular_layout)
# visual.draw_graph_k_paths(G_rand, paths_G_rand, nx.spring_layout, Reds[9])

#graphs = [grid_graph()]
graphs = load_graph_with_shortest_paths_list()
#print("graphs: ", graphs)
#print("graph: ", graphs[0])
#print("paths", graphs[0].shortest_paths[0][0])

#visual.draw_graph_k_paths(nx.DiGraph(graphs[0]), [paths for (paths, length) in graphs[0].shortest_paths], nx.circular_layout, Reds[9])
