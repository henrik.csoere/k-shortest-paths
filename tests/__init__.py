"""
This package contains Unit-Tests for our implemented algorithm using `pytest`.
We run the tests with the same set of graphs against each algorithm. 

TODO: If there are two paths with equal lengths, then we have to make sure, tests are still positive, even though those paths are in a different order


See: https://pytest.readthedocs.io/en/latest/
"""

