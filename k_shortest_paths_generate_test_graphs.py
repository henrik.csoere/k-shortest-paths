#!/usr/bin/env python3
from k_shortest_paths.graph_generator import generate_graphs as gg
from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import save_graph_with_shortest_paths
from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import check_if_serialized_graphs_exist
from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import GRAPH_DIRECTORY_NAME


"""This script generates test graphs, defined by the variables `RANDOM_GRAPHS` and
`NEIGHBOURHOOD_GRAPHS` for testing the correctnes of our implementations.

If there already are some test-graphs inside our data folder, the executor is asked, whether they want to generate more test-graphs.
"""

# Change these two lists, if you want to generate different Graphs:

# generation of random graphs need 'n' and 'k' as parameters,
# where n is the number of nodes and k is the number of 
# shortest paths which should be pre-calculated.
# Therefore, this is a list of tuples (n, k)
RANDOM_GRAPHS = [(50, 20), (150, 50), (250, 75)]

# generation of neighbourhood graphs need 'n', 'm' and 'k' as parameters, 
# where n is the number of rows, m is the number of columns and k is the number of 
# shortest paths which should be pre-calculated.
# Therefore, this is a list of tuples (n, m, k)
NEIGHBOURHOOD_GRAPHS = [(10, 10, 20), (20, 20, 50), (40, 50, 200)]

# in case the user has run this script multiple times, we ask him, whether or not they want to generate further test-graphs.
print("Generating some test-graphs...\n\
    Feel free to change the generation of graphs by modifying 'k_shortest_paths_generate_test_graphs.py'.")

if check_if_serialized_graphs_exist("test*"):
    yesno = None
    while yesno != 'y' and yesno != 'n':
        yesno = str(input("\nSome test-graphs already exist inside '{}'-directory.\
        \nDo you want to continue generating further test-graphs? (y/n) ".format(GRAPH_DIRECTORY_NAME)))

    if yesno == 'n':
        exit()

for (n, k) in RANDOM_GRAPHS:
    save_graph_with_shortest_paths(gg.calculate_k_shortest_paths(gg.generate_random_graph(n), k, 1, n), n, k, generated_graph_type="random")

for (n, m, k) in NEIGHBOURHOOD_GRAPHS:
    save_graph_with_shortest_paths(gg.calculate_k_shortest_paths(gg.generate_neighbourhood_graph(n, m), k, 1, n*m), n*m, k, generated_graph_type="neighbourhood")

print("Test-graphs have been generated. Run tests using 'pytest'.")